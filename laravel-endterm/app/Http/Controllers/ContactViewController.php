<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ContactViewController extends Controller {

    public function fetch() {
        $users = DB::select('select * from contact');
        return view('admin.adminc',['users'=>$users]);
    }

    public function show($id) {
        $users = DB::select('select * from contact where id = ?',[$id]);
        return view('admin.crudadmin.editc',['users'=>$users]);
    }

    public function edit(Request $request,$id) {
        $source = $request->input('source');
        $info = $request->input('info');
        $url = $request->input('url');
        if($request->hasfile('icon')){
            $file = $request->file('icon');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('imgramen', $filename);
   
            DB::update('update contact set  img = ?, source = ?, info = ?, url = ? where id = ?',[$filename, $source, $info, $url, $id]);
         }
        echo "<div class = 'alert alert-success'>
        Successfully Updated
        </div>"; 
        $users = DB::select('select * from contact where id = ?',[$id]);
        return view('admin.crudadmin.editc',['users'=>$users]);
    }

    public function destroy($id) {
        DB::delete('delete from contact where id = ?',[$id]);
        echo "<div class='alert alert-danger' role='alert'>Succesfully Deleted <a href = '/admincontact'>Click Here</a></div>";
    }

    public function indexcontact() {
        $users = DB::select('select * from contact');
        return view('contact',['users'=>$users]);
    }
}