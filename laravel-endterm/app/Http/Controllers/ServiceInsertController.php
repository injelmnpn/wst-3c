<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ServiceInsertController extends Controller {
   public function insertform() {
      return view('admin.crudadmin.serviceadd');
   }
	
   public function insert(Request $request) {
      $services = $request->input('services');
      DB::insert('insert into service (services) values(?)',[$services]);

      echo "<div class = 'alert alert-success'>
           Successfully Added
         </div>";
        return view('admin.crudadmin.serviceadd');
    
   }
}
