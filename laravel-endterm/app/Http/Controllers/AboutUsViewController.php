<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AboutUsViewController extends Controller {

    public function fetcha() {
        $users = DB::select('select * from aboutus');
        return view('admin.admina',['users'=>$users]);
    }
    

    public function showa($id) {
        $users = DB::select('select * from aboutus where id = ?',[$id]);
        return view('admin.crudadmin.edita',['users'=>$users]);
    }

    public function edita(Request $request,$id) {
        $name1 = $request->input('name1');
        $name2 = $request->input('name2');
        $mess = $request->input('mess');
        $imgo = $request->input('imgo');
        $nameo = $request->input('nameo');
        $poso = $request->input('poso');
        $imgs = $request->input('imgs');
        $names = $request->input('names');
        $poss = $request->input('poss');
        DB::update('update aboutus set title1 = ?, title2 = ?, about = ?,  imgo = ?, nameo = ?, positiono = ?, imgs = ?, names = ?, positions = ? where id = ?',[$name1, $name2, $mess, $imgo, $nameo, $poso, $imgs, $names, $poss, $id]);
        echo "<div class = 'alert alert-success'>
        Successfully Updated
        </div>"; 
        $users = DB::select('select * from aboutus where id = ?',[$id]);
        return view('admin.crudadmin.edita',['users'=>$users]);
    }

    public function destroya($id) {
        DB::delete('delete from aboutus where id = ?',[$id]);
        echo "<div class='alert alert-danger' role='alert'>Succesfully Deleted <a href = '/adminabout'>Click Here</a></div>";
    }


    public function indexabout() {
        $users = DB::select('select * from aboutus');
        return view('about',['users'=>$users]);
    }

}