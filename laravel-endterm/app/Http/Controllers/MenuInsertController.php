<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class MenuInsertController extends Controller {
   public function insertform() {
      return view('admin.crudadmin.menuadd');
   }
	
   public function insert(Request $request) {
      $kmenu = $request->input('kmenu');
      if($request->hasfile('image')){
         $file = $request->file('image');
         $extension = $file->getClientOriginalExtension();
         $filename = time() . '.' . $extension;
         $file->move('imgramen', $filename);

         DB::insert('insert into menu (img, kmenu) values(?,?)',[$filename, $kmenu]);
      }
      echo "<div class = 'alert alert-success'>
              Successfully Added
            </div>";
      return view('admin.crudadmin.menuadd');
    
   }
}
