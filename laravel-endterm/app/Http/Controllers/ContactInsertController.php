<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ContactInsertController extends Controller {
   public function insertform() {
      return view('admin.crudadmin.contactadd');
   }
	
   public function insert(Request $request) {
      $source = $request->input('source');
      $info = $request->input('info');
      $url = $request->input('url');
      if($request->hasfile('icon')){
         $file = $request->file('icon');
         $extension = $file->getClientOriginalExtension();
         $filename = time() . '.' . $extension;
         $file->move('imgramen', $filename);

         DB::insert('insert into contact (img, source, info, url) values(?,?,?,?)',[$filename, $source, $info, $url]);
      }
      echo "<div class = 'alert alert-success'>
           Successfully Added
         </div>"; 
        return view('admin.crudadmin.contactadd');
   }
}
