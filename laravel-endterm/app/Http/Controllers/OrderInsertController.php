<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class OrderInsertController extends Controller {
   public function insertorder() {
      $users = DB::select('select * from ordering');
        return view('order',['users'=>$users]);
   }
	
   public function insert(Request $request) {
      $name = $request->input('name');
      $num = $request->input('num');
      $add = $request->input('add');
      $order = $request->input('order');
      $qty = $request->input('qty');
      $date = $request->input('date');
      $status = $request->input('status');
      DB::insert('insert into ordering (fullname, number, address, ordername, qty, date, status) values(?,?,?,?,?,?,?)',[$name, $num, $add, $order, $qty, $date, $status]);
    
      echo "<div class = 'alert alert-success'>
           Successfully Order
         </div>"; 
         $users = DB::select('select * from ordering');
         return view('order',['users'=>$users]);
   }
}
