<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ServiceViewController extends Controller {

    public function index() {
        $users = DB::select('select * from service');
        return view('admin.admins',['users'=>$users]);
    }

    public function sshow($id) {
        $users = DB::select('select * from service where id = ?',[$id]);
        return view('admin.crudadmin.edits',['users'=>$users]);
    }

    public function sedit(Request $request,$id) {
        $services = $request->input('services');
        DB::update('update service set services = ? where id = ?',[$services, $id]);
        echo "<div class = 'alert alert-success'>
        Successfully Updated
        </div>"; 
        $users = DB::select('select * from service where id = ?',[$id]);
        return view('admin.crudadmin.edits',['users'=>$users]);
    }

    public function sdestroy($id) {
        DB::delete('delete from service where id = ?',[$id]);
        echo "<div class='alert alert-danger' role='alert'>Succesfully Deleted <a href = '/adminservice'>Click Here</a></div>";
    }

    public function indexservice() {
        $users = DB::select('select * from service');
        return view('services',['users'=>$users]);
    }
}