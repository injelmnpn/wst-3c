<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AboutUsInsertController extends Controller {
   public function insertform() {
      return view('admin.crudadmin.aboutadd');
   }
	
   public function insert(Request $request) {
    $name1 = $request->input('name1');
    $name2 = $request->input('name2');
    $mess = $request->input('mess');
    $imgo = $request->input('imgo');
    $nameo = $request->input('nameo');
    $poso = $request->input('poso');
    $imgs = $request->input('imgs');
    $names = $request->input('names');
    $poss = $request->input('poss');
      DB::insert('insert into aboutus (title1, title2, about, imgo, nameo, positiono, imgs, names, positions) values(?,?,?,?,?,?,?,?,?)',[$name1, $name2, $mess, $imgo, $nameo, $poso, $imgs, $names, $poss]);
      echo "<div class = 'alert alert-success'>
           Successfully Added
         </div>";
        return view('admin.crudadmin.aboutadd');
    
   }
}
