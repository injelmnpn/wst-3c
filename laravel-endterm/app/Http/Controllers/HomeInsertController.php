<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeInsertController extends Controller {
   public function insertform() {
      return view('admin.crudadmin.homeadd');
   }
	
   public function insert(Request $request) {
      $name1 = $request->input('name1');
      $name2 = $request->input('name2');
      $logo = $request->file('logo');
      $messimg = $request->file('messimg');
      $tagline = $request->input('tagline');
      $message = $request->input('message');
      $address = $request->input('address');
      if($request->hasfile('imgs')){
         $file = $request->file('imgs');
         $extension = $file->getClientOriginalExtension();
         $filename = time() . '.' . $extension;
         $file->move('imgramen', $filename);

         DB::insert('insert into home (name1, name2, logo, tagline, messimg, message, img, address) values(?,?,?,?,?,?,?)',[$name1, $name2, $logo, $tagline, $messimg, $message, $filename, $address]);
      }
      echo "<div class = 'alert alert-success'>
           Successfully Added
         </div>";
        return view('admin.crudadmin.homeadd');
    
   }
}
