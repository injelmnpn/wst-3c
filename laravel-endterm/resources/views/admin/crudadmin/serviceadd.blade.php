<!DOCTYPE html>
<html>
<title>GAREJI RAMEN-admin</title>
<link rel="icon" href="imgramen/logo.png" type="image/gif/png/jpg/jpeg">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-colors-highway.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
       :root {
        --rm-primary: #970000;
        --rm-dark: #121314;
        --rm-light: #FFF;
        --rm-muted: #7D7D7D;
        }

        * {
            font-family: 'Rajdhani', sans-serif;
            font-size: 18px;
        }

        .rm-display-1 {
            font-family: 'Segoe Script';
        }
        .button{
            height: 30px;
            width: 30px;
        }
        button{
            padding: 10px 16px;
            border-radius: 35px;
        }
        a{
            font-size: 14px;
        }
</style>
<body>
<div class="w3-sidebar w3-black w3-bar-block w3-card w3-animate-left" style="width:20%">
    <img src="imgramen/logo.png" class="rounded mx-auto d-block" alt="" width="80" height="80">
    <figure class="text-center">
        <h3 class="rm-display-1">Gareji Ramen</h3>
    </figure>
    <br>
    <hr>
        <a href="/adminhome" class="w3-bar-item w3-button">Home</a>
    <hr>
        <a href="/adminabout" class="w3-bar-item w3-button">About Us</a>
    <hr>
        <a href="/adminmenu" class="w3-bar-item w3-button">Menu</a>
    <hr>
        <a href="/adminservice" class="w3-bar-item w3-button" style="background-color:grey;">Services</a>
    <hr>
        <a href="/admincontact" class="w3-bar-item w3-button">Contacts</a>
</div>

<br>
    <div style="margin-left:28%">
        <div class='container'>
            <div class='row'>
                <div class='text-left'>
                    <a href="/adminservice"><button class="w3-button w3-highway-red" style="float: right;"><img src="imgramen/back-button.png" class="button" alt=""></button></a>
                </div>
            </div>
        </div>
            <div class="container mt-3">
            <p class="fs-4 fw-bold">ADD CONTENTS</p>
                    <form action = "/insertservice" method = "post" enctype="multipart/form-data">
                    <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                
                    <div class="col-10">
                        <label for="inputimg" class="form-label">Service:</label>
                        <input type="text" class="form-control" name='services' required>
                        <br><br>
                    </div>

            
                    <div class="d-grid gap-2 col-6 mx-auto">
                        <button type="submit" class="btn btn-dark mt-3 mb-3">Add Content</button>
                    </div>
                </form>
                <br>
            </div>
    </div>       
</body>
</html>  