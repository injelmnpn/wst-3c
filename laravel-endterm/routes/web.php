<?php

use Illuminate\Support\Facades\Route;
// use App\Http\Controllers\PostsController;
// use App\Http\Controllers\StudentController;
// use App\Http\Controllers\Student2Controller;
// use App\Http\Controllers\IndexController;
// // use App\Http\Controllers\OrderController;
use App\Http\Controllers\ValidationController;
// // use App\Http\Controllers\AdminController;
// use App\Http\Controllers\StudInsertController;
// use App\Http\Controllers\StudViewController;
// use App\Http\Controllers\AboutController;
// use App\Http\Controllers\RegisterController;
// use App\Http\Controllers\AppointmentController;
// use App\Http\Controllers\AppViewController;
// use App\Http\Controllers\StudUpdateController;
// use App\Http\Controllers\StudDeleteController;

use App\Http\Controllers\LoginController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\HomeInsertController;
use App\Http\Controllers\HomeViewController;
use App\Http\Controllers\HomeEditController;

use App\Http\Controllers\AboutUsInsertController;
use App\Http\Controllers\AboutUsViewController;

use App\Http\Controllers\AboutOwnersInsertController;

use App\Http\Controllers\MenuInsertController;
use App\Http\Controllers\MenuViewController;


use App\Http\Controllers\ServiceController;
use App\Http\Controllers\ServiceInsertController;
use App\Http\Controllers\ServiceViewController;

use App\Http\Controllers\ContactController;
use App\Http\Controllers\ContactInsertController;
use App\Http\Controllers\ContactViewController;

use App\Http\Controllers\OrderViewController;
use App\Http\Controllers\OrderInsertController;


use App\Http\Controllers\CKEditorController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// USER
Route::get('/',[HomeViewController::class, 'indexhome']);

Route::get('/about',[AboutUsViewController::class, 'indexabout']);

Route::get('/menu',[MenuViewController::class, 'indexmenu']);

Route::get('/services',[ServiceViewController::class, 'indexservice']);

Route::get('/contact',[ContactViewController::class, 'indexcontact']);

Route::get('/adminorder',[OrderViewController::class, 'indexorder']); //view
Route::get('/order',[OrderInsertController::class, 'insertorder']); //insert
Route::post('insertorder',[OrderInsertController::class, 'insert']); //insert
Route::get('oedit/{id}',[OrderViewController::class, 'oshow']); //edit
Route::post('oedit/{id}',[OrderViewController::class, 'oedit']); //edit
Route::get('odelete/{id}',[OrderViewController::class, 'odestroy']); //delete


// ADMIN
//LOGIN
Route::get('/adminlogin',[ValidationController::class, 'showramen']);
Route::post('/adminhome',[ValidationController::class, 'validateramen']);


//home
Route::get('/adminhome',[HomeViewController::class, 'index']); //view
Route::get('homeadd',[HomeInsertController::class, 'insertform']); //insert
Route::post('inserthome',[HomeInsertController::class, 'insert']); //insert
Route::get('editt/{id}',[HomeViewController::class, 'showw']); //edit
Route::post('editt/{id}',[HomeViewController::class, 'editt']); //edit
Route::get('deletee/{id}',[HomeViewController::class, 'destroyy']); //delete


//about
Route::get('/adminabout',[AboutUsViewController::class, 'fetcha']); //view
Route::get('aboutadd',[AboutUsInsertController::class, 'insertform']); //insert
Route::post('insertabout',[AboutUsInsertController::class, 'insert']); //insert
Route::get('edita/{id}',[AboutUsViewController::class, 'showa']); //edit
Route::post('edita/{id}',[AboutUsViewController::class, 'edita']); //edit
Route::get('deletea/{id}',[AboutUsViewController::class, 'destroya']); //delete



//menu
Route::get('/adminmenu',[MenuViewController::class, 'index']); //view
Route::get('menuadd',[MenuInsertController::class, 'insertform']); //insert
Route::post('insertmenu',[MenuInsertController::class, 'insert']); //insert
Route::get('medit/{id}',[MenuViewController::class, 'mshow']); //edit
Route::post('medit/{id}',[MenuViewController::class, 'medit']); //edit
Route::get('mdelete/{id}',[MenuViewController::class, 'mdestroy']); //delete


//service
Route::get('/adminservice',[ServiceViewController::class, 'index']); //view
Route::get('serviceadd',[ServiceInsertController::class, 'insertform']); //insert
Route::post('insertservice',[ServiceInsertController::class, 'insert']); //insert
Route::get('sedit/{id}',[ServiceViewController::class, 'sshow']); //edit
Route::post('sedit/{id}',[ServiceViewController::class, 'sedit']); //edit
Route::get('sdelete/{id}',[ServiceViewController::class, 'sdestroy']); //delete



//contact
Route::get('/admincontact',[ContactViewController::class, 'fetch']); //view
Route::get('contactadd',[ContactInsertController::class, 'insertform']); //insert
Route::post('insertcontact',[ContactInsertController::class, 'insert']); //insert
Route::get('edit/{id}',[ContactViewController::class, 'show']); //edit
Route::post('edit/{id}',[ContactViewController::class, 'edit']); //edit
Route::get('delete/{id}',[ContactViewController::class, 'destroy']); //delete















































































































// //LOGIN
// Route::get('/validation',[ValidationController::class, 'showform']);
// Route::post('/appointment',[ValidationController::class, 'validateform']);


// //register
// Route::get('register',[RegisterController::class, 'insertform']);
// Route::post('create',[RegisterController::class, 'insert']);


// //appointment
// Route::get('appoint',[AppointmentController::class, 'insertappoint']);
// Route::post('addappoint',[AppointmentController::class, 'appoint']);


// Route::get('appview',[AppViewController::class, 'displayAppointment']);
// Route::get('appviewuser',[AppViewController::class, 'displayApp']);



//demo
// Route::get('fckeditor', [CKEditorController::class, "fckeditor"]);
// Route::post('fckeditor/upload', [CKEditorController::class, "upload"])->name('fckeditor.image-upload');
// Route::post('fckeditorStore', [CKEditorController::class, "ckeditorStore"]);



//CRUD

// Route::get('/homeadd',[StudViewController::class, 'insertform']);
// Route::post('/create',[StudViewController::class, 'insert']);
// Route::get('edit-records',[StudUpdateController::class, 'index']);
// Route::get('edit/{id}',[StudUpdateController::class, 'show']);
// Route::post('edit/{id}',[StudUpdateController::class, 'edit']);
// Route::get('delete-records',[StudDeleteController::class, 'index']);
// Route::get('delete/{id}',[StudDeleteController::class, 'destroy']);

// Route::get('/customer/{customerid}/{name}/{address}', [OrderController::class, 'display']);  
// Route::get('/item/{itemno}/{name}/{price}', [OrderController::class, 'displayy']);  
// Route::get('/order/{customerid}/{name}/{orderno}/{date}', [OrderController::class, 'displayyy']);  
// Route::get('/orderdetails/{transno}/{orderno}/{itemid}/{name}/{price}/{qty}', [OrderController::class, 'displayyyy']);  

// Route::get('/contact', function(){  
//     return view('contact',['name'=>'John']);  
// });  
 
// Route::get('/about', function(){
//     return view('/about', [PostsController::class, 'display']);
// });

// Route::get('/details', [PostsController::class, 'details']);

// Route::get('/studentdetails', [StudentController::class, 'display']);


// Route::get('/studentview/{id?}', [Student2Controller::class, 'display']);  

// Route::get('/control/{id?}', function($id=null){
//     return view('control');
// }); 

// Route::get('/detalye', [IndexController::class, 'display']);



// Route::get('/post', [PostsController::class, 'index']);
// Route::get('/post/create', [PostsController::class, 'create']);
// Route::get('/post/angel', [PostsController::class, 'angel']);
// Route::get('/angel', 'App\Http\Controllers\PostsController@index');
// Route::get('/postangel/{id}', [PostsController::class, 'show']);



// Route::get('/dashboard', function () {
//     return view('dashboard');
// });



// Route::get('/activity2', function () {
//      return view('activity2');
// });

// Route::get('/activity1', function () {
//     return view('activity1');
// });

// Route::get('/about', function () {
//     return view('about');
// });

// Route::get('/reg', function () {
//     return view('reg');
// });

// Route::get('/login', function () {
//     return view('login');
// });

// Route::get('/contact', function () {
//     return view('contact');
// });

// Route::get('/welcome', function(){
//     retuen view('welcome');
// });





// Route::get('/item/{itemno}/{name}/{price}', function ($itemno, $name, $price) {
//     return "ITem No.: ". $itemno . "<br>" . "Name: " . $name . "<br>" . "Price: " . $price;
// });

// Route::get('/costumer/{id}/{name}/{address}/{age?}', function($id, $name, $address, $age=null){
//     return "ID: ". $id . "<br>" . "Name: " . $name . "<br>" . "Address: " . $address . "<br>" . "Age: " . $age;
// });

// Route::get('/order/{customerid}/{name}/{orderno}/{date}', function ($customerid, $name, $orderno, $date) {
//     return "Customer ID: ". $customerid . "<br>" . "Name: " . $name . "<br>" . "Order No. : " . $orderno . "<br>" . "Date: " . $date;
// });

// Route::get('/orderdetails/{transno}/{orderno}/{itemid}/{name}/{price}/{qty}/{receiptnum?}', function ($transno, $orderno, $itemid, $name, $price, $qty, $receiptnum=null) {
//     return  "Trans No.: ". $transno . "<br>" . "Order No.: " . $orderno . "<br>" . "Item Id: " . $itemid . "<br>" . "Name: " . $name. "<br>" . "Price: " . $price. "<br>" . "Qty: " . $qty. "<br>" . "Receipt Num: " . $receiptnum. "<br>" . "TOTAL PRICE: " . $price * $qty;
// });


// Route::get('/student',function()  
// {  
//   return view('student');  
// }); 


// Route::get('student/details',function()  
// {  
// 	$url="Name: Angel Joy B. Manipon" . "<br>" . "Section: 3C";  
// 	return $url;  

// })->name('student.details');  



// Route::redirect('/here', '/there', 404);



// Route::get('/student', function(){
//     return view('student');
// });

// Route::get('student/details',function()  
// {  
//     $url=route('student.details');  
//     return $url;  
// })->name('student.details');  

// Route::get('/about', function()  
// {  
//  return "This is a about us page";   
// });  

// Route::get('/post/{id}/{name}', function($id, $name)  
// {  
//     return "id number is : ". $id. " " . $name;   
// }  
// );  

// Route::get('user/{name?}', function ($name=null) {  
//     return $name;  
// });  
    
// Route::get('user/{name?}', function ($name = 'himani') {  
// 	    return $name;  
// });  
    

