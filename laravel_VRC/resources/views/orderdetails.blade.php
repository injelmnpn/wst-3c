<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ORDERDETAILS</title>
    <link rel="icon" href="imgramen/logo.png" type="image/gif/png/jpg/jpeg">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>
        :root {
        --rm-primary: #970000;
        --rm-dark: #121314;
        --rm-light: #FFF;
        --rm-muted: #7D7D7D;
        }

        * {
            font-family: 'Rajdhani', sans-serif;
            font-size: 18px;
        }

        .rm-bg-dark {
            background: var(--rm-dark);
        }
    </style>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark rm-bg-dark">
        <div class="container">
          <a class="navbar-brand" href="#"><img src="imgramen/gareji.png" width="250" alt=""></a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end" id="navbarNavAltMarkup">
            <div class="navbar-nav">
              <a class="nav-link p-lg-3" aria-current="page" href="/item/{itemno}/{name}/{price}">ITEM</a>
              <a class="nav-link p-lg-3" href="/customer/{customerid}/{name}/{address}">CUSTOMER</a>
              <a class="nav-link p-lg-3" href="/order/{customerid}/{name}/{orderno}/{date}">ORDER</a>
              <a class="nav-link p-lg-3 active" href="/orderdetails/{transno}/{orderno}/{itemid}/{name}/{price}/{qty}">ORDER DETAILS</a>
            </div>
          </div>
        </div>
      </nav>

      <br><br>
    <div class="container">
        <div class="card">
                <div class="card-body">
                    <p class="fs-3 text-center fw-normal">ORDER DETAILS PAGE</p>
                </div>
        </div>
    </div>

    <br>
    <div class="container">
        <center>
        <div class="card" style="width: 18rem;">
                <div class="card-body">
                    <p class="card-text"> 
                    <p>TRANS NO: <?= $transno?></p>
                    <p>ORDER NO: <?= $orderno?></p>
                    <p>ITEM ID: <?= $itemid?></p>
                   <p>NAME: <?= $name?></p>
                   <p>PRICE: <?= $price?></p>
                   <p>QTY: <?= $qty?></p>
                </div>
        </div>
        </center>
    </div>
</body>
</html>


