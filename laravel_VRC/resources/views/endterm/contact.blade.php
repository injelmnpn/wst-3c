<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>GAREJI RAMEN</title>
    <link rel="icon" href="imgramen/logo.png" type="image/gif/png/jpg/jpeg">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>
        :root {
        --rm-primary: #970000;
        --rm-dark: #121314;
        --rm-light: #FFF;
        --rm-muted: #7D7D7D;
        }

        * {
            font-family: 'Rajdhani', sans-serif;
            font-size: 18px;
        }

        .rm-bg-dark {
            background: var(--rm-dark);
        }

        .rm-display-1 {
            font-size: 1.5rem;
            font-weight: bold;
            font-family: 'Segoe Script';
            color: var(--rm-primary);
        }

        .rm-display-3 {
            font-size: 5rem;
            font-weight: 700;
        }

        .rm-display-5 {
            font-size: 1.5rem;
            font-weight: 500;
            font-family: 'Rajdhani', sans-serif;
        }

        .rm-display-55 {
            font-weight: bold;
            font-family: 'Segoe Script';
        }

        .rm-display-4 {
            font-size: 3rem;
            font-weight: normal;
            font-family: 'Segoe Script';
        }

        .rm-display-44 {
            color: var(--rm-dark);
            font-size: 1.5rem;
            font-weight: bold;
            font-family: 'Rajdhani', sans-serif;
        }

        .rm-text-dark {
            color: var(--rm-dark);
            font-family: 'Segoe Script';
        }

        .rm-text-primary {
            color: var(--rm-primary);
            font-family: 'Segoe Script';
        }

        .cards{
            background-color: var(--rm-primary);
            border-radius: 5px;
        }

        .rm-text-muted {
            color: var(--rm-muted);
        }

        .rm-text-medium {
            font-weight: 500;
        }

        .rm-text-bold {
            font-weight: 700;
        }

        .rm-footer {
            background: var(--rm-primary);
        }

        .image{
            border-radius: 270px;
            box-shadow: 2px 2px;
            width: 450px;
            height: 450px;
        }

        .rm-display-11 {
            font-size: 1.3rem;
            font-weight: normal;
            font-family: 'Segoe Script';
        }
    </style>
</head>
<body>
     <nav class="navbar navbar-expand-lg navbar-dark rm-bg-dark">
        <div class="container">
          <a class="navbar-brand" href="#"><img src="imgramen/gareji.png" width="250" alt=""></a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end" id="navbarNavAltMarkup">
            <div class="navbar-nav">
              <a class="nav-link p-lg-3" aria-current="page" href="/">HOME</a>
              <a class="nav-link p-lg-3" href="/about">ABOUT</a>
              <a class="nav-link p-lg-3" href="/menu">MENU</a>
              <a class="nav-link p-lg-3" href="/services">SERVICES</a>
              <a class="nav-link p-lg-3  active" href="/contact">CONTACT</a>
            </div>
          </div>
        </div>
      </nav>

    <br><br>
      <div class="container">
      <div class="card">
            <div class="card-body">
                <h1 class="rm-display-1 text-center rm-text-light">Contacts</span></h1>
                <h3 class="rm-display-4 text-center text-dark">Order Now</h3>
            </div>
        </div>
      </div>

      <br><br>
      <div class="container">
      <div class="card">
            <div class="card-body">
                <h1 class="rm-display-11 text-left rm-text-light">Contact Us</span></h1>
            </div>

            <br><br>
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-1">
                        <img src="imgramen/email.jpg" alt="" width="30" height="30"> 
                    </div>

                    <div class="col-5">
                        <b><?=' Email'?></b>
                        <br>
                        <?=' angeljoymanipon@gmail.com'?>
                    </div>
                    
                    <div class="col-1">
                        <img src="imgramen/facebook.png" alt="" width="25" height="20"> 
                    </div>
                    <div class="col-5">
                        <b><?=' Facebok Page'?></b>
                        <br>
                        <?=' #275 Zone 5 Labit Proer Urdaneta City, Pangasinan'?>
                    </div>
                </div>
                <br>
                <div class="row align-items-center"> 
                    <div class="col-1">
                        <img src="imgramen/add.png" alt="" width="30" height="30"> 
                    </div>
                    <div class="col-5">
                        <b><?=' Address'?></b>
                        <br>
                        <?=' #275 Zone 5 Labit Proer Urdaneta City, Pangasinan'?>
                    </div>
                </div>
                <br>
                <div class="row align-items-center"> 
                    <div class="col-1">
                        <img src="imgramen/phone.jpg" alt="" width="35" height="35"> 
                    </div>

                    <div class="col-5">
                        <b><?=' Phone'?></b>
                        <br>
                        <?=' 09663283591'?>
                    </div>
                </div>
            </div>
            <br><br>
        </div>
      </div>

      <br><br><br>
    <!-- Footer -->
    <div class="container-fluid rm-footer d-flex flex-column justify-content-center">
        <h1 class="rm-display-5 text-center text-light rm-text-bold mt-5">GAREJI RAMEN</h1>
        <p class="container text-center text-light">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quis, numquam nesciunt ipsum aut nam repudiandae nostrum earum sed architecto quae obcaecati vitae aliquam! Repellendus aperiam recusandae tempore nobis ratione esse, error beatae mollitia repellat sunt, quas debitis. Sit, possimus aut.</p>
        <p class="text-light text-center">Copyright 2022. All rights reserved. <br> Design by Angel Joy Manipon </p>
    </div>

</body>
</html>