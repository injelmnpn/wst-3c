<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Personal Information-AM</title>
    <link rel="icon" href="image/logo.png" type="image/gif/png">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>
  :root {
    --rm-primary: #FF4C00;
    --rm-dark: #121314;
    --rm-light: #FFF;
    --rm-muted: #7D7D7D;
    --rm-blue: #82CAFA;
    --rm-bluee: #B4CFEC;
    --rm-wel: #0669b2;
    --rm-come: #00a8ec;
    }
    img{
        margin-left: 40px;
    }
    a{
        text-decoration: none;
        color: rgba(34,54,69,.7);
        font-weight: 500;
    }
    a:hover{
        color: #ADD8E6;
    }
    ul{
        list-style-type: none;
    }
    nav{
      box-shadow: 0 0 10px rgba(0,0,0,0.8);
    }
    .navbar .navbar-brand a{
        padding: 1rem 0;
        text-decoration: none;
    }
    .navbar-toggler{
        background: #ADD8E6;
        border: none;
        padding: 10px 6px;
    }
    .navbar-toggler span{
        display: block;
        width: 22px;
        height: 2px;
        border: 1px;
        background: #fff;
    }
    .navbar-toggler span + span{
      margin-top: 4px;
      width: 18px;
      margin-left: 4px;
    }
    .navbar-toggler span + span + span {
      margin-top: 10px;
      margin-left: 1px;
    }
    .navbar-expand-lg .navbar-nav .nav-link{
        padding: 2rem 1.2rem;
        font-size: 1rem;
        position: relative;
    }
    .navbar-expand-lg .navbar-nav .nav-link:hover{
        border-top: 4px solid #ADD8E6;
    }
    .navbar-expand-lg .navbar-nav .nav-link:.active{
        border-top: 4px solid #ADD8E6;
        color: #ADD8E6;
    }
    .rm-hero {
    background: url(./assets/images/hero-1.jpg);
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    height: 70vh;
    }
    .img{
      margin-top: 100px;
      width: 350px;
      height: 350px;
    }
    .imagee{
      margin-left: 10px;
      margin-right: 10px;
      height: 570px;
    }
    .rm-bg-dark {
    background: var(--rm-light);
    }
    .rm-bg-light {
    background: var(--rm-bluee);
    }
    .rm-hero {
    background: url(image/gif2.jpg);
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    height: 86vh;
    margin-top: 20px;
    }
    .rm-heroo {
    background: url(image/gif.webp);
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    height: 80vh;
    }
    .rm-display-7 {
    font-size: 7rem;
    font-weight: 700;
    margin-top: 10px;
    }
    .rm-text-light {
    font-size: 20px;;
    color: var(--rm-blue);
    }
    .rm-text-primary {
    color: var(--rm-dark);
    }
    .rm-display-3 {
    font-size: 5rem;
    font-weight: 700;
    color: var(--rm-dark);
    }
    .rm-display-9 {
    font-size: 5rem;
    font-weight: 700;
    color: var(--rm-blue);
    }
    .rm-display-2 {
    font-family: 'san-serif';
    font-size: 60px;
    font-weight: 700;
    margin-top: 150px;
    }
    .rm-display-4 {
    font-size: 3rem;
    font-weight: normal;
    color: var(--rm-light);
    font-family: Arial, Helvetica, sans-serif;
    }
    .rm-display-0 {
    font-size: 1rem;
    font-weight: normal;
    color: var(--rm-dark);
    font-family: 'sans-serif';
    font-style: italic;
    }
    .rm-display-11 {
    font-size: 1rem;
    font-weight: normal;
    color: var(--rm-dark);
    font-family: Arial, Helvetica, sans-serif;
    }
    .rm-display-12 {
    font-size: 3rem;
    font-weight: normal;
    color: var(--rm-dark);
    font-family: Arial, Helvetica, sans-serif;
    }
    .card-img-top{
      margin-left: 0;
    }
    .footer-copyright {
    background: var(--rm-bluee);
    }
    .card-titlee{
      margin-top: 41px;
    }
    .foot{
      margin-bottom: 13px;
    }
    .card{
      margin-top: 20px;
      margin-left: 20px;
      margin-right: 20px;
    }
    .cardd{
      margin-top: 20px;
      margin-left: 20px;
      margin-right: 20px;
    }
    .carddd{
      margin: 0 auto; /* Added */
        float: none; /* Added */
        margin-bottom: 10px; /* Added */
    }
    h3{
      font-size: 22px;
      font-family: Arial, Helvetica, sans-serif;
    }
    .rm-display-22 {
    font-family: 'san-serif';
    font-size: 40px;
    font-weight: 700;
    margin-top: 150px;
    font-style: italic;
    }
    .rm-display-44{
    font-size: 20px;
    font-weight: normal;
    color: var(--rm-light);
    }
</style>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="#"><img src="image/logo.png" width="70" heigth="70"></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span></span>
      <span></span>
      <span></span>
    </button>
    <div class="collapse navbar-collapse jstify-content-between" id="navbarSupportedContent">
      <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link" href="/">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/about">About</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/reg">Registration</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/login">Login</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="#" href="#">Contact</a>
        </li>
      </ul>
      <ul class="navbar-nav ms-auto">
    
      </ul>
    </div>
  </div>
</nav>
<div class="card">
  <div class="card-body">
          <h1 class="rm-display-1 text-center rm-text-light">CONTACT</span></h1>
          <h3 class="rm-display-4 text-center text-dark">Get In Touch</h3>
          <h1 class="rm-display-0 text-center rm-text-light">with me</span></h1>
  </div>
</div>

<div class="cardd">
<div class="card shadow-sm p-3 mb-5 bg-body rounded">
  <div class="card-body">
  <h1 class="rm-display-11 text-left rm-text-light">Get In Touch</span></h1>
  <h3 class="rm-display-12 text-left text-dark">Let's Communicate Wisely</h3>

  <br><br>
  <div class="container">
  <div class="row align-items-center">
    <div class="col-1">
      <img src="image/email.png" alt="" width="30" height="30"> 
    </div>
      <div class="col-5">
      <?=' Email'?>
      <br>
      <?=' angeljoymanipon@gmail.com'?>
    </div>
    <div class="col-1">
      <img src="image/phone.png" alt="" width="30" height="30"> 
    </div>
    <div class="col-5">
    <?=' Phone'?>
    <br>
      <?=' 09663283591'?>
    </div>
  </div>
  <br>
  <div class="row align-items-center">
    <div class="col-1">
      <img src="image/add.png" alt="" width="30" height="30"> 
    </div>
    <div class="col-5">
      <?=' Address'?>
      <br>
      <?=' #275 Zone 5 Labit Proer Urdaneta City, Pangasinan'?>
    </div>
  </div>
</div>
</div>
</div>
</div>

<div class="container">
  <div class="row align-items-center">
    <div class="col">
    </div>
    <div class="col">
    <div class="carddd">
      <div class="card" style="width: 50rem;">
        <div class="card-header">
          Suggestion/Comments
        </div>
      <div class="shadow p-3 mb-5 bg-body rounded">
        <div class="card-body">
          <div class="row g-3">
            <div class="col-5">
              <input type="text" class="form-control" placeholder="Name" aria-label="name">
            </div>
          </div>
        <br>
      <div class="row g-3">
        <div class="col-5">
          <input type="text" class="form-control" placeholder="Email" aria-label="First name">
        </div>
      </div>
      <br>
      <div class="form-floating">
        <textarea class="form-control" placeholder="Leave a suggestion here" id="floatingTextarea"></textarea>
        <label for="floatingTextarea">Comments</label>
      </div>
    </div>
  </div>
  <div class="d-grid gap-2 col-6 mx-auto">
      <button class="btn btn-primary" type="button"><a href="/contact" style="color: white;">Submit</a></button>
      <br>
    </div>
</div>
</div>
    </div>
    <div class="col">
    </div>
  </div>
</div>

<br><br><br>
<div class="card">
  <div class="card-body">
          <h1 class="rm-display-1 text-center rm-text-light">NICE PICTURES</span></h1>
          <h3 class="rm-display-4 text-center text-dark">GALLERY</h3>
  </div>
</div>
<div class="container">
  <div class="row align-items-center">
    <div class="col">
    <div class="card">
      <div class="card-body">
        <img src="image/img4.jpg" alt="" width="170" heigth="170" class="align-items-center"> 
      </div>
    </div>
    </div>
    <div class="col">
    <div class="card">
      <div class="card-body">
      <img src="image/img1.jpg" alt="" width="220" heigth="220" class="align-items-center"> 
      </div>
    </div>
    </div>
    <div class="col">
    <div class="card">
      <div class="card-body">
        <img src="image/img8.jpg" alt="" width="170" heigth="250" class="align-items-center"> 
      </div>
    </div>
    </div>
  </div>

  <div class="row align-items-center">
    <div class="col">
    <div class="card">
      <div class="card-body">
        <img src="image/img6.jpg" alt="" width="220" heigth="220" class="align-items-center"> 
      </div>
    </div>
    </div>
    <div class="col">
    <div class="card">
      <div class="card-body">
        <img src="image/img7.jpg" alt="" width="220" heigth="220" class="align-items-center"> 
      </div>
    </div>
    </div>
    <div class="col">
    <div class="card">
      <div class="card-body">
        <img src="image/img9.jpg" alt="" width="220" heigth="220" class="align-items-center"> 
      </div>
    </div>
    </div>
  </div>
</div>

<br><br><br>
  

<footer class="page-footer font-small blue pt-4">
  <div class="container-fluid text-center text-md-left">
    <div class="row justify-content-center" style="background: var(--rm-blue);">
      <div class="col-2">
            <br>
            <a href="#!"><img src="image/fb.png" width="30" heigth="30"></a>
      </div>

      <div class="col-2">
            <br>
            <a href="#!"><img src="image/google.png" width="30" heigth="30"></a>
      </div>

      <div class="col-2">
            <br>
            <a href="#!"><img src="image/instagram.png" width="30" heigth="30" class="foot"></a>
      </div>
    </div>
  </div>

  <div class="footer-copyright text-center py-3">© 2022 Copyright:
    <a href="/"> Designed by: Angel Joy B. Manipon | Power by <b>Laravel</b></a>
  </div>
</footer>
</body>
</html>
