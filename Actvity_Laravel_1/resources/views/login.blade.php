<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Personal Information-AM</title>
    <link rel="icon" href="image/logo.png" type="image/gif/png">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<style>
  :root {
    --rm-primary: #FF4C00;
    --rm-dark: #121314;
    --rm-light: #FFFFFF;
    --rm-muted: #7D7D7D;
    --rm-blue: #82CAFA;
    --rm-bluee: #B4CFEC;
    --rm-wel: #0669b2;
    --rm-come: #00a8ec;
    }
    img{
        margin-left: 40px;
    }
    a{
        text-decoration: none;
        color: rgba(34,54,69,.7);
        font-weight: 500;
    }
    a:hover{
        color: #ADD8E6;
    }
    ul{
        list-style-type: none;
    }
    .navbar{
        background: white;
        padding: 3rem 2rem;
        height: 0rem;
        min-height: 12vh;
        box-shadow: 0 0 10px rgba(0,0,0,0.8);
    }
    .navbar .navbar-brand a{
        padding: 1rem 0;
        display: block;
        text-decoration: none;
    }
    .navbar-toggler{
        background: #ADD8E6;
        border: none;
        padding: 10px 6px;
        outline: none;
    }
    .navbar-toggler span{
        display: block;
        width: 22px;
        height: 2px;
        border: 1px;
        background: #fff;
    }
    .navbar-toggler span + span{
      margin-top: 4px;
      width: 18px;
      margin-left: 4px;
    }
    .navbar-toggler span + span + span {
      margin-top: 10px;
      margin-left: 1px;
    }
    .navbar-expand-lg .navbar-nav .nav-link{
        padding: 2rem 1.2rem;
        font-size: 1rem;
        position: relative;
    }
    .navbar-expand-lg .navbar-nav .nav-link:hover{
        border-top: 4px solid #ADD8E6;
    }
    .navbar-expand-lg .navbar-nav .nav-link:.active{
        border-top: 4px solid #ADD8E6;
        color: #ADD8E6;
    }

    .main-wrap{
    width: 100%;
    height: 100vh;
    display: flex;
    justify-content: center;
    align-items: center;
    }
    .box-container{
    display: flex;
    background: #fff;
    }
    .img-box{
    width: 800px;
    background: url(image/gif2.gif);
    }
    .form-wrap{
    width: 50%;
    height: 100%;
    padding: 20px;
    }
    .form-wrap .top-signup{
    text-align: right;
    padding-top: 10px;
    font-size: 13px;
    font-weight: 400;
    color: rgb(155, 152, 152);  
    }
    .form-wrap .signup-btn{
    text-decoration: none;
    color: rgb(155, 152, 152);
    border: 1px solid rgb(182, 181, 181);
    padding: 4px 8px;
    font-size: 12px;
    margin-left: 8px;
    border-radius: 20px;
    }
    .form-wrap .signup-btn:hover{
    background-color: rgba(238, 238, 238, 0.527);
    }
    .form-wrap .mid-container{
    padding-top: 40px;
    padding-left: 40px;
    }
    .mid-container h6{
    font-size: 16px;
    font-weight: 400;
    padding: 8px 2px;
    letter-spacing: 0.002rem;
    color: rgb(155, 152, 152);
    }
    .mid-container .form{
    padding-top: 26px;
    }
    .mid-container .form label{
    font-weight: 500;
    }
    .mid-container .form input{
    width: 320px;
    margin: 6px 0px;
    height: 33px;
    color: rgb(155, 152 152);
    padding-left: 5px;
    border-radius: 4px;
    border: 1px solid rgb(216, 215, 215);
    }
    .mid-container .form input:focus{
    border: 1px solid rgb(182, 180, 180);
    outline: none;
    }
    .mid-container .form input::placeholder{
    color: rgb(175, 174, 174);
    }
    .fg-pass{
    color: rgb(175, 174, 174);
    font-size: 14px;
    text-decoration: none;
    }
    .mid-container .form .login-btn{
    padding: 10px;
    cursor: pointer;
    width: 140px;
    font-size: 16px;
    margin-top: 20px;
    background-color: rgb(68, 138, 230);
    color: rgb(247, 244, 244);
    border-radius: 20px;
    border: none;
    }
    .mid-container .form .login-btn a{
    text-decoration: none;
    color: white;
    }
    .mid-container .form .login-btn:hover{
    background-color: rgba(39, 39, 175, 0.938);
    transition: all ease 1s;
    }
    .login-with{
    padding: 50px 40px;
    }
    .login-with span{
    padding: 8px;
    width: 25px;
    height: 25px;
    margin: 4px 5px;
    }
    .login-with a img{
    width: 25px;
    height: 25px;
    margin-bottom: -3px;
    }
    .footer-copyright {
    background: var(--rm-bluee);
    }
    .card-titlee{
      margin-top: 41px;
    }
    .foot{
      margin-bottom: 13px;
    }
</style>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="#"><img src="image/logo.png" width="70" heigth="70"></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span></span>
      <span></span>
      <span></span>
    </button>
    <div class="collapse navbar-collapse jstify-content-between" id="navbarSupportedContent">
      <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link" href="/">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/about">About</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/reg">Registration</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="#" href="#">Login</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/contact">Contact</a>
        </li>
      </ul>
      <ul class="navbar-nav ms-auto">
    
      </ul>
    </div>
  </div>
</nav>

<div class="main-wrap">
        <div class="box-container">
          <div class="img-box">
          </div>
            <div class="form-wrap">
                <div class="top-signup">
                    <span>You want to Join?</span>
                    <a href="/reg" class="signup-btn">Registration</a>
                </div>
                <div class="mid-container">
                    <h1>Welcome</h1>
                    <h6>Login your account</h6>
                    <form action="" class="form">
                        <label for="username">Username</label><br>
                        <input for="username" id="uname" placeholder="Your email"><br><br>
                        <label for="password">Password</label><br>
                        <input type="password" name="password" id="pass" placeholder="Your password">
                        <span><a href="#" class="fg-pass">Forgot password?</a></span>                   
                        <br>
                        <button type="submit" class="login-btn"><a href="/">Login</a></button>
                    </form>
                    <div class="login-with">
                        <span>Login with</span>
                        <a href="/"><img src="image/facebook.png"></a>
                        <a href="/"><img src="image/google.jpg"></a>
                        <a href="/"><img src="image/instagram_icon.png"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer class="page-footer font-small blue pt-4">
  <div class="footer-copyright text-center py-3">© 2022 Copyright:
    <a href="/"> Designed by: Angel Joy B. Manipon | Power by <b>Laravel</b></a>
  </div>
</footer>
</body>
</html>
