<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Repository in Bitbucket</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>
<br><br>
<div class="container"> 
  <div class="row">
    <div class="col-9">
        <div class="shadow p-5 mb-5 bg-body rounded">
            <div class="mb-3 row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Name:</label>
                <div class="col-sm-10">
                    <input type="text" readonly class="form-control-plaintext"  value="Manipon, Angel Joy B">
                </div>
            </div>

            <div class="mb-3 row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Year & Section:</label>
                <div class="col-sm-10">
                    <input type="text" readonly class="form-control-plaintext"  value="3rd Year - Section C">
                </div>
            </div>

            <div class="mb-3 row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Subject:</label>
                <div class="col-sm-10">
                    <input type="text" readonly class="form-control-plaintext"  value="Elective 1(Web System and Technologies 2)">
                </div>
            </div>

            <div class="mb-3 row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Date:</label>
                <div class="col-sm-10">
                    <input type="text" readonly class="form-control-plaintext"  value="March 01, 2022">
                </div>
            </div>

            <div class="mb-3 row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Time:</label>
                <div class="col-sm-10">
                    <input type="text" readonly class="form-control-plaintext"  value="12:28pm">
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
</body>
</html>