-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 15, 2022 at 04:25 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `endterm`
--

-- --------------------------------------------------------

--
-- Table structure for table `aboutus`
--

CREATE TABLE `aboutus` (
  `id` int(11) NOT NULL,
  `title1` varchar(800) NOT NULL,
  `title2` varchar(800) NOT NULL,
  `about` varchar(800) NOT NULL,
  `imgo` varchar(500) NOT NULL,
  `nameo` varchar(800) NOT NULL,
  `positiono` varchar(800) NOT NULL,
  `imgs` varchar(500) NOT NULL,
  `names` varchar(800) NOT NULL,
  `positions` varchar(800) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `aboutus`
--

INSERT INTO `aboutus` (`id`, `title1`, `title2`, `about`, `imgo`, `nameo`, `positiono`, `imgs`, `names`, `positions`) VALUES
(2, 'Gareji', 'Ramen', 'Gareji Ramen is a store that serve a ramen food and other dishes such sushi rolls, rice bowl and salad. The Gareji Ramen build since the store spot is “nasa mismong Garahe” we derived the store name in the tagalog word “Garahe” which translates to “Gareji” in Japanese.', 'owners.jpg', 'April Lorenzo & Francis Abriam', 'Owners', 'stafff.jpg', 'Justin Diaz/ Kurt Russel, Lorieann Hermoso & Jeral Castaneda', 'All Around Cook Sushi Chef/ Kitchen Aide Sushi Chef/ Kitchen Aide');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `img` varchar(800) NOT NULL,
  `source` varchar(800) NOT NULL,
  `info` varchar(800) NOT NULL,
  `url` varchar(900) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `img`, `source`, `info`, `url`) VALUES
(1, '1655200219.jpg', 'Email', 'francisabriam@gmail.com', 'https://francisabriam@gmail.com'),
(2, '1655271525.png', 'Facebook', 'Gareji Ramen-Bayoas', 'https://www.facebook.com/garejiramen'),
(5, '1655279717.png', 'Contact No.', '0945910180/09171324988', '/contact');

-- --------------------------------------------------------

--
-- Table structure for table `home`
--

CREATE TABLE `home` (
  `id` int(11) NOT NULL,
  `name1` varchar(800) NOT NULL,
  `name2` varchar(800) NOT NULL,
  `logo` varchar(600) NOT NULL,
  `tagline` varchar(800) NOT NULL,
  `messimg` varchar(500) NOT NULL,
  `message` varchar(800) NOT NULL,
  `img` varchar(500) NOT NULL,
  `address` varchar(800) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `home`
--

INSERT INTO `home` (`id`, `name1`, `name2`, `logo`, `tagline`, `messimg`, `message`, `img`, `address`) VALUES
(2, 'Gareji', 'Ramen', 'logo.jpg', 'Try it once, you get hooked for life. What a massive noodle bowl! When life gives you ramen, you slurp it. Great taste in every slurp.', 'welcome.jpg', 'Hi Customer!\r\nWe are very happy to welcome you on in Gareji Ramen! \r\nYou always welcome here.  We will be happy \r\nto help you and to service you.\r\n\r\nAs a thank you for joining us, I would like to give \r\nyou a Ramen.', '1655200157.png', 'Bayaoas Urdaneta City, Pangasinan');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `img` varchar(800) NOT NULL,
  `kmenu` varchar(800) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `img`, `kmenu`) VALUES
(1, '1655200322.jpg', 'TOP MENU'),
(6, '1655277768.jpg', 'SUSHI & RICE BOWL MENU'),
(7, '1655277780.jpg', 'ALL MENU');

-- --------------------------------------------------------

--
-- Table structure for table `ordering`
--

CREATE TABLE `ordering` (
  `id` int(11) NOT NULL,
  `fullname` varchar(600) NOT NULL,
  `number` varchar(255) NOT NULL,
  `address` varchar(500) NOT NULL,
  `ordername` varchar(500) NOT NULL,
  `qty` int(100) NOT NULL,
  `date` date NOT NULL,
  `status` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ordering`
--

INSERT INTO `ordering` (`id`, `fullname`, `number`, `address`, `ordername`, `qty`, `date`, `status`) VALUES
(3, 'Angel Joy Manipon', '09663283591', 'Labit Proper', 'ramen', 4, '2022-06-15', 'CANCEL ORDER');

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `id` int(11) NOT NULL,
  `services` varchar(800) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`id`, `services`) VALUES
(1, 'Opening Hours: 10:30AM - 10:00PM'),
(2, 'We offer Authentic Japanese Style Ramen and other dishes'),
(5, 'We also accept deliveries, pick up and dine-in.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aboutus`
--
ALTER TABLE `aboutus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home`
--
ALTER TABLE `home`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ordering`
--
ALTER TABLE `ordering`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aboutus`
--
ALTER TABLE `aboutus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `home`
--
ALTER TABLE `home`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `ordering`
--
ALTER TABLE `ordering`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `service`
--
ALTER TABLE `service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
