<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>GAREJI RAMEN</title>
    <link rel="icon" href="imgramen/logo.png" type="image/gif/png/jpg/jpeg">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>
        :root {
        --rm-primary: #970000;
        --rm-dark: #121314;
        --rm-light: #FFF;
        --rm-muted: #7D7D7D;
        }

        * {
            font-family: 'Rajdhani', sans-serif;
            font-size: 18px;
        }

        .rm-bg-dark {
            background: var(--rm-dark);
        }

        .rm-display-1 {
            font-size: 7rem;
            font-weight: 700;
        }

        .rm-display-3 {
            font-size: 5rem;
            font-weight: 700;
        }

        .rm-display-33 {
            font-size: 1.5rem;
            font-weight: bold;
            font-family: sans-serif;
        }

        .rm-display-34 {
            font-size: 1.5rem;
            font-weight: bold;
            font-family: sans-serif;
            color: var(--rm-primary);
        }

        .rm-display-5 {
            font-size: 1.5rem;
            font-weight: 500;
            font-family: 'Rajdhani', sans-serif;
        }

        .rm-display-55 {
            font-weight: bold;
            font-family: 'Segoe UI';
        }

        .rm-display-4 {
            color: var(--rm-primary);
        }

        .rm-display-44 {
            color: var(--rm-dark);
            font-size: 1.5rem;
            font-weight: bold;
            font-family: 'Rajdhani', sans-serif;
        }

        .rm-display-6 {
            color: var(--rm-dark);
            font-size: 1.5rem;
            font-family: 'Segoe Script';
        }

        .rm-display-7 {
            color: var(--rm-primary);
            font-size: 1.5rem;
            font-family: 'Segoe Script';
        }

        .rm-display-8 {
            font-size: 1.5rem;
            font-family: 'Rajdhani', sans-serif;
        }

        .rm-display-9 {
            font-size: 1.5rem;
            font-family: 'Rajdhani', sans-serif;
        }

        .rm-text-dark {
            color: var(--rm-dark);
            font-family: 'Segoe Script';
        }

        .rm-text-primary {
            color: var(--rm-primary);
            font-family: 'Segoe Script';
        }

        .cards{
            background-color: var(--rm-primary);
            border-radius: 5px;
        }

        .rm-text-muted {
            color: var(--rm-muted);
        }

        .rm-text-medium {
            font-weight: 500;
        }

        .rm-text-bold {
            font-weight: 700;
        }

        .rm-footer {
            background: var(--rm-primary);
        }

        .image{
            border-radius: 670px;
            width: 280px;
            height: 280px;
            box-shadow: 2px 2px;
        }
        .col{
            margin-top: 30px;
        }
    </style>
</head>
<body>
      <nav class="navbar navbar-expand-lg navbar-dark rm-bg-dark">
        <div class="container">
          <a class="navbar-brand" href="#"><img src="imgramen/gareji.png" width="250" alt=""></a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end" id="navbarNavAltMarkup">
            <div class="navbar-nav">
              <a class="nav-link p-lg-3" aria-current="page" href="/">HOME</a>
              <a class="nav-link p-lg-3 active" href="/about">ABOUT</a>
              <a class="nav-link p-lg-3" href="/menu">MENU</a>
              <a class="nav-link p-lg-3" href="/services">SERVICES</a>
              <a class="nav-link p-lg-3" href="/contact">CONTACT</a>
            </div>
          </div>
        </div>
      </nav>

    <!-- Hero -->
    <div class="container p-5">
      <div class="row">
            <div class="col p-xl-5">
                <h3 class="rm-display-3 rm-text-dark mt-5">Gareji <br><span class="rm-display-3 rm-text-primary">Ramen</span> </h3>     
            </div>
            <div class="col"> 
              <h3 class="rm-display-33">About<span class="rm-display-34">Us</span></h3>  
                  <div class="card shadow p-3 mb-5 bg-white rounded">
                    <div class="card-body">   
                        <p class="rm-text-muted rm-text-medium">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                        sed do eiusmod tempor incididunt ut labore et dolore
                        magna aliqua. Nisi quis eleifend quam adipiscing vitae
                        proin sagittis nisl rhoncus. Euismod nisi porta lorem 
                        mollis aliquam ut. Ultrices neque ornare aenean euismod 
                        elementum nisi quis eleifend quam. Magna fermentum 
                        iaculis eu non. Lobortis feugiat vivamus at augue eget 
                        arcu dictum.
                        </p>
                    </div>
                  </div>  
            </div>
        </div>
    </div>

    <br><br>

    <div class="container">
          <div class="container-lg">
              <h3 class="rm-display-3 text-center text-dark">The <span class="rm-text-primary rm-display-3">Owners</span> </h3>
          </div>

            <div class="row m-4">
                  <div class="col-lg p-4">
                        <img src="imgramen/owner1.jpg" class="image mx-auto d-block">
                        <br>
                        <h5 class="rm-display-6 text-center text-dark">April Lorenzo</h5>
                  </div>
                  <div class="col-lg p-4">
                        <img src="imgramen/owner2.jpg" class="image mx-auto d-block">
                        <br>
                        <h5 class="rm-display-7 text-center rm-primary">Francis Abriam</h5>
                  </div>
            </div>
    </div>
    <br><br>

    <div class="container-fluid rm-bg-dark pt-4">
          <div class="container-lg">
              <div class="row m-4">
                  <div class="col-lg p-4">
                    <div class="card rm-bg-dark">
                        <div class="card-body">
                            <h3 class="rm-display-8 text-center text-light">GOOD <span class="rm-text-primary rm-display-9">FOOD</span> </h3>
                            <h3 class="rm-display-8 text-center rm-text-primary">GOOD <span class="text-light rm-display-9">RAMEN</span> </h3>
                            <br>
                        </div>
                      </div>
                  </div>
                  <div class="col-2">
                    <div class="card rm-bg-dark">
                        <div class="card-body">
                          
                        </div>
                      </div>
                  </div>
                  <div class="col-2">
                    <div class="card rm-bg-dark">
                        <div class="card-body">
                          
                        </div>
                      </div>
                  </div>
                  <div class="col-2">
                    <div class="card rm-bg-dark">
                        <div class="card-body">
                          
                        </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>

      <br><br><br>

      <div class="container">
          <div class="container-lg">
              <h3 class="rm-display-3 text-center text-dark">Our <span class="rm-text-primary rm-display-3">Staffs</span> </h3>
          </div>

            <div class="row m-4">
                  <div class="col-lg p-4">
                        <img src="imgramen/owner1.jpg" class="image mx-auto d-block">
                        <br>
                        <h5 class="rm-display-6 text-center text-dark">April Lorenzo</h5>
                  </div>
                  <div class="col-lg p-4">
                        <img src="imgramen/owner2.jpg" class="image mx-auto d-block">
                        <br>
                        <h5 class="rm-display-7 text-center rm-primary">Francis Abriam</h5>
                  </div>
                  <div class="col-lg p-4">
                        <img src="imgramen/owner2.jpg" class="image mx-auto d-block">
                        <br>
                        <h5 class="rm-display-7 text-center rm-primary">Francis Abriam</h5>
                  </div>
            </div>
    </div>


    <br><br><br><br>
    <!-- Footer -->
    <div class="container-fluid rm-footer d-flex flex-column justify-content-center">
        <h1 class="rm-display-5 text-center text-light rm-text-bold mt-5">GAREJI RAMEN</h1>
        <p class="text-light text-center">Copyright 2022. All rights reserved. <br> Design by Angel Joy Manipon </p>
    </div>
</body>
</html>