<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>GAREJI RAMEN</title>
    <link rel="icon" href="imgramen/logo.png" type="image/gif/png/jpg/jpeg">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>
        :root {
        --rm-primary: #970000;
        --rm-dark: #121314;
        --rm-light: #FFF;
        --rm-muted: #7D7D7D;
        }

        * {
            font-family: 'Rajdhani', sans-serif;
            font-size: 18px;
        }

        .rm-bg-dark {
            background: var(--rm-dark);
        }

        .rm-display-1 {
            font-size: 7rem;
            font-weight: 700;
        }

        .rm-display-3 {
            font-size: 5rem;
            font-weight: 700;
        }

        .rm-display-5 {
            font-size: 1.5rem;
            font-weight: 500;
            font-family: 'Rajdhani', sans-serif;
        }

        .rm-display-13 {
            font-size: 3.5rem;
            font-weight: bold;
            font-family: 'Rajdhani', sans-serif;
        }

        .rm-display-14 {
            font-size: 2rem;
            font-weight: bold;
            font-family: 'Rajdhani', sans-serif;
            color: var(--rm-primary);
        }

        .rm-display-15 {
            font-size: 2rem;
            font-weight: bold;
            font-family: 'Rajdhani', sans-serif;
        }

        .rm-display-55 {
            font-weight: bold;
            font-family: 'Segoe UI';
        }

        .rm-display-4 {
            color: var(--rm-primary);
        }

        .rm-display-44 {
            color: var(--rm-dark);
            font-size: 1.5rem;
            font-weight: bold;
            font-family: 'Rajdhani', sans-serif;
        }

        .rm-text-dark {
            color: var(--rm-dark);
            font-family: 'Segoe Script';
        }

        .rm-text-primary {
            color: var(--rm-primary);
            font-family: 'Segoe Script';
        }

        .cards{
            background-color: var(--rm-primary);
            border-radius: 5px;
        }

        .rm-text-muted {
            color: var(--rm-muted);
        }

        .rm-text-medium {
            font-weight: 500;
        }

        .rm-text-bold {
            font-weight: 700;
        }

        .rm-footer {
            background: var(--rm-primary);
        }

        .image{
            border-radius: 270px;
            box-shadow: 2px 2px;
            width: 250px;
            height: 250px;
        }
        .menu{
            background-color: var(--rm-primary);
        }
        h3 {
        text-align: left;
        }
    </style>
</head>
<body>
     <nav class="navbar navbar-expand-lg navbar-dark rm-bg-dark">
        <div class="container">
          <a class="navbar-brand" href="#"><img src="imgramen/gareji.png" width="250" alt=""></a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end" id="navbarNavAltMarkup">
            <div class="navbar-nav">
              <a class="nav-link p-lg-3" aria-current="page" href="/">HOME</a>
              <a class="nav-link p-lg-3" href="/about">ABOUT</a>
              <a class="nav-link p-lg-3 active" href="/menu">MENU</a>
              <a class="nav-link p-lg-3" href="/services">SERVICES</a>
              <a class="nav-link p-lg-3" href="/contact">CONTACT</a>
            </div>
          </div>
        </div>
      </nav>

      <!-- Hero -->
    <div class="container p-5">
      <div class="row">
            <div class="col p-xl-5">
                <h3 class="rm-display-3 rm-text-dark mt-5">Gareji <br><span class="rm-display-3 rm-text-primary">Ramen</span> </h3>     
            </div>
            <div class="col"> 
              <img src="imgramen/menu.png" class="rounded mx-auto d-block" alt="" width="450" heigth="450">
            </div>
        </div>
    </div>

    <!-- Nav -->
    <div class="menu">
        <br>
            <div class="container-lg">
                <h3 class="rm-display-13  text-center text-light mt-5">OUR <span class="rm-display-13 text-dark">TOP</span> <span class="rm-display-13 text-light">MENU</span></h3>     
            </div>

            <div class="row m-4">
                  <div class="col-lg p-4">
                        <img src="imgramen/owner1.jpg" class="image mx-auto d-block">
                        <br>
                        <h5 class="rm-display-6 text-center text-light">April Lorenzo</h5>
                  </div>
                  <div class="col-lg p-4">
                        <img src="imgramen/owner2.jpg" class="image mx-auto d-block">
                        <br>
                        <h5 class="rm-display-7 text-center text-light">Francis Abriam</h5>
                  </div>
                  <div class="col-lg p-4">
                        <img src="imgramen/owner2.jpg" class="image mx-auto d-block">
                        <br>
                        <h5 class="rm-display-7 text-center text-light">Francis Abriam</h5>
                  </div>
            </div>
    </div>

    <!-- ramen menu -->
    <div class="container-fluid">
        <div class="container-lg">
            <div class="d-flex justify-content-start">
                    <h3 class="rm-display-15  text-center text-dark mt-5">RAMEN <span class="rm-display-14">MENU</span></h3>     
            </div>
                <div class="row m-4">

                  <div class="col-lg p-2">
                    <div class="card" style="width: 18rem;">
                      <img class="card-img-top" src="imgramen/welcome.jpg" width="300" height="300">
                      <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg p-2">
                    <div class="card" style="width: 18rem;">
                      <img class="card-img-top" src="imgramen/welcome.jpg" width="300" height="300">
                      <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg p-2">
                    <div class="card" style="width: 18rem;">
                      <img class="card-img-top" src="imgramen/welcome.jpg" width="300" height="300">
                      <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg p-2">
                    <div class="card mb-3" style="max-width: 540px;">
                        <div class="row g-0">
                            <div class="col-md-4">
                            <img src="imgramen/welcome.jpg" class="img-fluid rounded-start" width="300" height="300">
                            </div>
                            <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                            </div>
                            </div>
                        </div>
                    </div>
                  </div>

                </div>
        </div>
    </div>

    <!-- sushi rolls -->
    <div class="container-fluid">
        <div class="container-lg">
            <div class="d-flex justify-content-start">
                    <h3 class="rm-display-15  text-center text-dark mt-5">SUSHI <span class="rm-display-14">ROLLS</span></h3>     
            </div>
                <div class="row m-4">

                  <div class="col-lg p-2">
                    <div class="card" style="width: 18rem;">
                      <img class="card-img-top" src="imgramen/welcome.jpg" width="300" height="300">
                      <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg p-2">
                    <div class="card" style="width: 18rem;">
                      <img class="card-img-top" src="imgramen/welcome.jpg" width="300" height="300">
                      <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg p-2">
                    <div class="card" style="width: 18rem;">
                      <img class="card-img-top" src="imgramen/welcome.jpg" width="300" height="300">
                      <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                      </div>
                    </div>
                </div>

                </div>
        </div>
    </div>


    <!-- salad -->
    <div class="container-fluid">
        <div class="container-lg">
            <div class="d-flex justify-content-start">
                    <h3 class="rm-display-15  text-center text-dark mt-5">SAL<span class="rm-display-14">AD</span></h3>     
            </div>
            <div class="row">
                    <div class="col">
                        <div class="card">
                        <img src="..." class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                        </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                        <img src="..." class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                        </div>
                        </div>
                    </div>

                </div>
        </div>
    </div>

<br>

    <!-- rice bowl -->
    <div class="container-fluid">
        <div class="container-lg">
            <div class="d-flex justify-content-start">
                    <h3 class="rm-display-15  text-center text-dark mt-5">RICE <span class="rm-display-14">BOWLS</span></h3>     
            </div>

            <div class="row row-cols-1 row-cols-md-2 g-4">
                <div class="col">
                    <div class="card">
                    <img src="..." class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                    </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card">
                    <img src="..." class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                    </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card">
                    <img src="..." class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content.</p>
                    </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card">
                    <img src="..." class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<br>

      <!-- add ons -->
      <div class="container-fluid">
        <div class="container-lg">
            <div class="d-flex justify-content-start">
                    <h3 class="rm-display-15  text-center text-dark mt-5">ADD <span class="rm-display-14">ONS</span></h3>     
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Special title treatment</h5>
                        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Special title treatment</h5>
                        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Special title treatment</h5>
                        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-sm-4">
                    <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Special title treatment</h5>
                        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Special title treatment</h5>
                        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Special title treatment</h5>
                        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

    <br>

    <!-- drinks -->
    <div class="container-fluid">
        <div class="container-lg">
            <div class="d-flex justify-content-start">
                    <h3 class="rm-display-15  text-center text-dark mt-5">DR<span class="rm-display-14">INKS</span></h3>     
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Special title treatment</h5>
                        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Special title treatment</h5>
                        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                    </div>
                </div>
            </div>
        
        </div>
    </div>

    <br><br><br><br>
    <!-- Footer -->
    <div class="container-fluid rm-footer d-flex flex-column justify-content-center">
        <h1 class="rm-display-5 text-center text-light rm-text-bold mt-5">GAREJI RAMEN</h1>
        <p class="text-light text-center">Copyright 2022. All rights reserved. <br> Design by Angel Joy Manipon </p>
    </div>
</body>
</html>