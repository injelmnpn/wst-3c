<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Consultation</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="main.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="main.js"></script>
    <style>
        body{
    font-family: 'Roboto', sans-serif;
  }
  #hero {
      width: 100%;
      height: 80vh;
      background: url("./images/hero.jpg") center center;
      background-size: cover;
      position: relative;
    }
    
    #hero .container {
      padding-top: 80px;
    }
    
    #hero:before {
      content: "";
      background: rgba(0, 0, 0, 0.6);
      position: absolute;
      bottom: 0;
      top: 0;
      left: 0;
      right: 0;
    }
    
    #hero h1 {
      margin: 0 0 10px 0;
      font-size: 48px;
      font-weight: 700;
      line-height: 56px;
      color: #0d98ba;
    }
    
    #hero h2 {
      color: #eee;
      margin-bottom: 40px;
      font-size: 15px;
      font-weight: 500;
      font-family: "Open Sans", sans-serif;
      letter-spacing: 0.5px;
      text-transform: uppercase;
    }
    
    .btn{
      width: 500px;
    }
    .navbar{
      background-color: black;
    }
    .nav-link{
      color: white;
    }
    .footer {
    left: 0;
    bottom: 0;
    width: 100%;
    background-color: black;
    color: white;
    text-align: center;
  }
    </style>
  </head>
  <body>
      <nav class="navbar navbar-expand-lg navbar-dark rm-bg-dark">
        <a class="nav-link active" aria-current="page"  href="#">Appointment</a>
        <a class="nav-link active" aria-current="page"  href="/appviewuser">List of Appointment</a>
        <a class="nav-link" href="/validation">Logout</a>
      </nav>
   <section id="hero" class="d-flex align-items-center">
    <div class="container text-center position-relative">
      <h1>24/7 Care is available</h1>
      <h2>THank You for Trusting Us</h2>
    </div>
  </section>
 

  <div class="container">
               <br>
              <h1 class="display-5 text-center">Appointment Form</h1>
              <br>
              <div class="card shadow p-3 mb-5 bg-body rounded">
                <div class="card-body">
                  <form action = "/addappoint" method = "Post">
                    <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                    <div class="mb-3">
                      <label for="name" class="form-label">Full Name</label>
                      <input type="text" class="form-control" placeholder="First Name, Middle Initial, Last Name" name="patname" required>
                    </div>
                    <div class="mb-3">
                      <label for="contact" class="form-label">Contact</label>
                      <input type="text" class="form-control" placeholder="123" name="contact" required>
                    </div>
                    <div class="mb-3">
                      <label for="date" class="form-label">Date</label>
                      <input type="date" class="form-control" name="date" required>
                    </div>
                    <div class="mb-3">
                      <label for="time" class="form-label">Time</label>
                      <input type="text" class="form-control" placeholder="8:00am-12:am" name="time" required>
                    </div>
                    <div class="c0l-6">
                      <label for="symptoms" class="form-label">Symptoms</label>
                      <textarea id="symptoms" class="form-control" name="symptoms" required></textarea>
                    </div>
                    <br>
                    <div class="d-grid gap-2 col-6 mx-auto">
                              <button type="submit" name="submit" class="btn btn-success">
                                  Confirm
                              </button>
                    </div>
                  </form>
              </div>
            </div>    
  </div>
<br><br><br>
<div class="footer">
    <div class="footer-copyright text-center py-3">© 2020 Copyright: Manipon, Angel Joy</div>
  </div> 
</body>
</html>