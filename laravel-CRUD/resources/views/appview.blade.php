<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Appointment-Admin</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="main.js"></script>
    <style>
        body{
    font-family: 'Roboto', sans-serif;
        }
  #hero {
      width: 100%;
      height: 80vh;
      background: url("./images/hero.jpg") center center;
      background-size: cover;
      position: relative;
    }
    
    .navbar{
      background-color: black;
    }
    .nav-link{
      color: white;
    }

    #hero .container {
      padding-top: 80px;
    }
    
    #hero:before {
      content: "";
      background: rgba(0, 0, 0, 0.6);
      position: absolute;
      bottom: 0;
      top: 0;
      left: 0;
      right: 0;
    }
    
    #hero h1 {
      margin: 0 0 10px 0;
      font-size: 48px;
      font-weight: 700;
      line-height: 56px;
      color: #fff;
    }
    
    #hero h2 {
      color: #eee;
      margin-bottom: 40px;
      font-size: 15px;
      font-weight: 500;
      font-family: "Open Sans", sans-serif;
      letter-spacing: 0.5px;
      text-transform: uppercase;
    }
    
    .main-btn {
      font-family: "Poppins", sans-serif;
      text-transform: uppercase;
      font-weight: 500;
      font-size: 14px;
      letter-spacing: 1px;
      display: inline-block;
      padding: 8px 28px;
      border-radius: 50px;
      transition: 0.5s;
      margin: 10px;
      border: 2px solid #fff;
      color: #fff;
    }
    
     .main-btn:hover {
      background: #11dbcf;
      border: 2px solid #11dbcf;
      text-decoration: none;
      color:#fff;
    }
   .form-group .btn-form{
    background: #11dbcf;
    border: 2px solid #11dbcf;
    text-decoration: none;
    color:#000;
   }
   .footer {
    left: 0;
    bottom: 0;
    width: 100%;
    background-color: black;
    color: white;
    text-align: center;
  }
    </style>
  </head>
  <body>
  <nav class="navbar navbar-expand-lg navbar-dark rm-bg-dark">
        <a class="nav-link active" aria-current="page"  href="#">Appointment</a>
        <a class="nav-link" href="/validation">Logout</a>
      </nav>
   <section id="hero" class="d-flex align-items-center">
    <div class="container text-center position-relative">
      <h1>24/7 Care is available</h1>
      <h2>THank You for Trusting Us</h2>
    </div>
  </section>
  <!-- End Hero -->
  <div class="container mt-4 p-4">
      <h2 class="text-center my-4">
         APPOINTMENT DETAILS
      </h2>

      <table class="table table-striped table-hover">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Name</th>
          <th scope="col">Contact</th>
          <th scope="col">Date</th>
          <th scope="col">Time</th>
          <th scope="col">Symptoms</th>
          <th scope="col"></th>
        </tr>
      </thead>
      <tbody>
      @foreach ($users as $user)
        <tr>
          <th scope="row">{{ $user->id }}</th>
          <td>{{ $user->patname }}</td>
          <td>{{ $user->contact }}</td>
          <td>{{ $user->date }}</td>
          <td>{{ $user->time }}</td>
          <td>{{ $user->symptoms }}</td>
          <td>
          <div class="d-grid gap-2 d-md-block">
            <button class="btn btn-outline-success" type="button">Approved</button>
            <button class="btn btn-outline-danger" type="button">Declined</button>
        </div>
        </td>
        </tr>
      @endforeach
      </tbody>
    </table>
  </div>
  <br><br>

  
  <div class="footer">
    <div class="footer-copyright text-center py-3">© 2020 Copyright: Manipon, Angel Joy</div>
  </div>  
</body>
</html>