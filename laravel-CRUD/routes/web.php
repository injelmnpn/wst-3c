<?php

use Illuminate\Support\Facades\Route;
// use App\Http\Controllers\PostsController;
// use App\Http\Controllers\StudentController;
// use App\Http\Controllers\Student2Controller;
// use App\Http\Controllers\IndexController;
// use App\Http\Controllers\OrderController;
use App\Http\Controllers\ValidationController;
// use App\Http\Controllers\AdminController;
use App\Http\Controllers\StudInsertController;
use App\Http\Controllers\StudViewController;
use App\Http\Controllers\AboutController;
// use App\Http\Controllers\StudUpdateController;
// use App\Http\Controllers\StudDeleteController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\AppointmentController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\AppViewController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//LOGIN
Route::get('/validation',[ValidationController::class, 'showform']);
Route::post('/appointment',[ValidationController::class, 'validateform']);


//register
Route::get('register',[RegisterController::class, 'insertform']);
Route::post('create',[RegisterController::class, 'insert']);


//appointment
Route::get('appoint',[AppointmentController::class, 'insertappoint']);
Route::post('addappoint',[AppointmentController::class, 'appoint']);


Route::get('appview',[AppViewController::class, 'displayAppointment']);
Route::get('appviewuser',[AppViewController::class, 'displayApp']);

















//LOGIN
// Route::get('/adminlogin',[ValidationController::class, 'showramen']);
// Route::post('/adminhome',[ValidationController::class, 'validateramen']);



// // USER
// Route::get('/',[StudViewController::class, 'indexx']);

// Route::get('/about', function () {
//     return view('about');
// });

// Route::get('/menu', function () {
//     return view('menu');
// });

// Route::get('/services', function () {
//     return view('services');
// });

// Route::get('/contact', function () {
//     return view('contact');
// });



// // ADMIN
// //dash
// Route::get('/admindash', function () {
//     return view('admin.admind');
// }); 
// Route::get('/dashedit', function () {
//     return view('admin.crudadmin.editd');
// });

// //home
// Route::get('/adminhome',[StudViewController::class, 'index']);
// Route::get('/homeadd', function () {
//     return view('admin.crudadmin.homeadd');
// });
// Route::get('/homeedit', function () {
//     return view('admin.crudadmin.edith');
// });

// //about
// Route::get('/adminabout',[StudViewController::class, 'view']);
// Route::get('/aboutadd', function () {
//     return view('admin.crudadmin.aboutadd');
// });
// Route::get('/aboutedit', function () {
//     return view('admin.crudadmin.edita');
// });
// Route::get('/about2edit', function () {
//     return view('admin.crudadmin.editos');
// });


// //menu
// Route::get('/adminmenu', function () {
//     return view('admin.adminm');
// });
// Route::get('/menuadd', function () {
//     return view('admin.crudadmin.menuadd');
// });

// Route::get('/menutopedit', function () {
//     return view('admin.crudadmin.editt');
// });
// Route::get('/menuramenedit', function () {
//     return view('admin.crudadmin.editr');
// });
// Route::get('/menusushiedit', function () {
//     return view('admin.crudadmin.editsu');
// });
// Route::get('/menusaladedit', function () {
//     return view('admin.crudadmin.editsa');
// });
// Route::get('/menuriceedit', function () {
//     return view('admin.crudadmin.editri');
// });
// Route::get('/menuaddedit', function () {
//     return view('admin.crudadmin.editad');
// });
// Route::get('/menudrinksedit', function () {
//     return view('admin.crudadmin.editdr');
// });

// //service
// Route::get('/adminservice', function () {
//     return view('admin.admins');
// });
// Route::get('/serviceadd', function () {
//     return view('admin.crudadmin.serviceadd');
// });
// Route::get('/serviceedit', function () {
//     return view('admin.crudadmin.edits');
// });

// //contact
// Route::get('/admincontact', function () {
//     return view('admin.adminc');
// });
// Route::get('/contactadd', function () {
//     return view('admin.crudadmin.contactadd');
// });
// Route::get('/contactedit', function () {
//     return view('admin.crudadmin.editc');
// });












































































































//CRUD

// Route::get('/homeadd',[StudViewController::class, 'insertform']);
// Route::post('/create',[StudViewController::class, 'insert']);
// Route::get('edit-records',[StudUpdateController::class, 'index']);
// Route::get('edit/{id}',[StudUpdateController::class, 'show']);
// Route::post('edit/{id}',[StudUpdateController::class, 'edit']);
// Route::get('delete-records',[StudDeleteController::class, 'index']);
// Route::get('delete/{id}',[StudDeleteController::class, 'destroy']);

// Route::get('/customer/{customerid}/{name}/{address}', [OrderController::class, 'display']);  
// Route::get('/item/{itemno}/{name}/{price}', [OrderController::class, 'displayy']);  
// Route::get('/order/{customerid}/{name}/{orderno}/{date}', [OrderController::class, 'displayyy']);  
// Route::get('/orderdetails/{transno}/{orderno}/{itemid}/{name}/{price}/{qty}', [OrderController::class, 'displayyyy']);  

// Route::get('/contact', function(){  
//     return view('contact',['name'=>'John']);  
// });  
 
// Route::get('/about', function(){
//     return view('/about', [PostsController::class, 'display']);
// });

// Route::get('/details', [PostsController::class, 'details']);

// Route::get('/studentdetails', [StudentController::class, 'display']);


// Route::get('/studentview/{id?}', [Student2Controller::class, 'display']);  

// Route::get('/control/{id?}', function($id=null){
//     return view('control');
// }); 

// Route::get('/detalye', [IndexController::class, 'display']);



// Route::get('/post', [PostsController::class, 'index']);
// Route::get('/post/create', [PostsController::class, 'create']);
// Route::get('/post/angel', [PostsController::class, 'angel']);
// Route::get('/angel', 'App\Http\Controllers\PostsController@index');
// Route::get('/postangel/{id}', [PostsController::class, 'show']);



// Route::get('/dashboard', function () {
//     return view('dashboard');
// });



// Route::get('/activity2', function () {
//      return view('activity2');
// });

// Route::get('/activity1', function () {
//     return view('activity1');
// });

// Route::get('/about', function () {
//     return view('about');
// });

// Route::get('/reg', function () {
//     return view('reg');
// });

// Route::get('/login', function () {
//     return view('login');
// });

// Route::get('/contact', function () {
//     return view('contact');
// });

// Route::get('/welcome', function(){
//     retuen view('welcome');
// });





// Route::get('/item/{itemno}/{name}/{price}', function ($itemno, $name, $price) {
//     return "ITem No.: ". $itemno . "<br>" . "Name: " . $name . "<br>" . "Price: " . $price;
// });

// Route::get('/costumer/{id}/{name}/{address}/{age?}', function($id, $name, $address, $age=null){
//     return "ID: ". $id . "<br>" . "Name: " . $name . "<br>" . "Address: " . $address . "<br>" . "Age: " . $age;
// });

// Route::get('/order/{customerid}/{name}/{orderno}/{date}', function ($customerid, $name, $orderno, $date) {
//     return "Customer ID: ". $customerid . "<br>" . "Name: " . $name . "<br>" . "Order No. : " . $orderno . "<br>" . "Date: " . $date;
// });

// Route::get('/orderdetails/{transno}/{orderno}/{itemid}/{name}/{price}/{qty}/{receiptnum?}', function ($transno, $orderno, $itemid, $name, $price, $qty, $receiptnum=null) {
//     return  "Trans No.: ". $transno . "<br>" . "Order No.: " . $orderno . "<br>" . "Item Id: " . $itemid . "<br>" . "Name: " . $name. "<br>" . "Price: " . $price. "<br>" . "Qty: " . $qty. "<br>" . "Receipt Num: " . $receiptnum. "<br>" . "TOTAL PRICE: " . $price * $qty;
// });


// Route::get('/student',function()  
// {  
//   return view('student');  
// }); 


// Route::get('student/details',function()  
// {  
// 	$url="Name: Angel Joy B. Manipon" . "<br>" . "Section: 3C";  
// 	return $url;  

// })->name('student.details');  



// Route::redirect('/here', '/there', 404);



// Route::get('/student', function(){
//     return view('student');
// });

// Route::get('student/details',function()  
// {  
//     $url=route('student.details');  
//     return $url;  
// })->name('student.details');  

// Route::get('/about', function()  
// {  
//  return "This is a about us page";   
// });  

// Route::get('/post/{id}/{name}', function($id, $name)  
// {  
//     return "id number is : ". $id. " " . $name;   
// }  
// );  

// Route::get('user/{name?}', function ($name=null) {  
//     return $name;  
// });  
    
// Route::get('user/{name?}', function ($name = 'himani') {  
// 	    return $name;  
// });  
    

