<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class RegisterController extends Controller {
   public function insertform() {
      return view('registration');
   }
	
   public function insert(Request $request) {
      $request->all();
      $this->validate($request,[
      'username'=>'required|max:8',
      'email'=>'required|email|unique:users',
      'password'=>'required'
  ]);
  return view('appointment');
   }
}