<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OrderController extends Controller
{
    //
    public function display($customerid, $name, $address)  
	{  
	    return view('customer',['customerid' => $customerid,'name' => $name,'address' => $address]);  
    }

    public function displayy($itemno, $name, $price)  
	{  
	    return view('item', ['itemno' => $itemno,'name' => $name,'price' => $price]);  
    }

    public function displayyy($customerid, $name, $orderno, $date)  
	{  
	    return view('order', ['customerid' => $customerid,'name' => $name,'orderno' => $orderno,'date' => $date]);  
    }

    public function displayyyy($transno, $orderno, $itemid, $name, $price, $qty)  
	{  
	    return view('orderdetails', ['transno' => $transno,'orderno' => $orderno,'itemid' => $itemid,'name' => $name,'price' => $price, 'qty' => $qty]);  
    }

}
