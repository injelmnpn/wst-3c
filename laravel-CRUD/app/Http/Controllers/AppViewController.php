<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AppViewController extends Controller {

   public function displayAppointment() {
      $users = DB::select('select * from appointments');
      return view('appview',['users'=>$users]);
   }

   public function displayApp() {
      $users = DB::select('select * from appointments');
      return view('appviewuser',['users'=>$users]);
   }
}
