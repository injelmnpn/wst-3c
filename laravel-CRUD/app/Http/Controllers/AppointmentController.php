<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AppointmentController extends Controller {
   public function insertappoint() {
      return view('appointment');
   }
	
   public function appoint(Request $request) {
      $patname = $request->input('patname');
      $contact = $request->input('contact');
      $date = $request->input('date');
      $time = $request->input('time');
      $symptoms = $request->input('symptoms');
      $users = DB::select('select * from appointments');

         if($users=4){
            DB::insert('insert into appointments (patname, contact, date, time, symptoms) values(?,?,?,?,?)',[$patname, $contact, $date, $time, $symptoms]);
            echo "<div class = 'alert alert-success'>
           Approve Appointment
         </div>";
         return view('appointment');
         }
         else{
            echo "<div class = 'alert alert-danger'>
            Only 4 Appointment in a Day
          </div>";
          return view('appointment');
         }
   }
}
