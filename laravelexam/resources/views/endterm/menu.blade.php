<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>GAREJI RAMEN</title>
    <link rel="icon" href="imgramen/logo.png" type="image/gif/png/jpg/jpeg">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>
        :root {
        --rm-primary: #970000;
        --rm-dark: #121314;
        --rm-light: #FFF;
        --rm-muted: #7D7D7D;
        }

        * {
            font-family: 'Rajdhani', sans-serif;
            font-size: 18px;
        }

        .rm-bg-dark {
            background: var(--rm-dark);
        }

        .rm-display-1 {
            font-size: 7rem;
            font-weight: 700;
        }

        .rm-display-3 {
            font-size: 5rem;
            font-weight: 700;
        }

        .rm-display-5 {
            font-size: 1.5rem;
            font-weight: 500;
            font-family: 'Rajdhani', sans-serif;
        }

        .rm-display-13 {
            font-size: 3.5rem;
            font-weight: bold;
            font-family: 'Rajdhani', sans-serif;
        }

        .rm-display-14 {
            font-size: 2rem;
            font-weight: bold;
            font-family: 'Rajdhani', sans-serif;
            color: var(--rm-primary);
        }

        .rm-display-15 {
            font-size: 2rem;
            font-weight: bold;
            font-family: 'Rajdhani', sans-serif;
        }

        .rm-display-20 {
            font-size: 2rem;
            font-weight: bold;
            font-family: 'Segoe Script';
            color: #fff;
        }

        .rm-display-55 {
            font-weight: bold;
            font-family: 'Segoe UI';
        }

        .rm-display-4 {
            color: var(--rm-primary);
        }

        .rm-display-44 {
            color: var(--rm-dark);
            font-size: 1.5rem;
            font-weight: bold;
            font-family: 'Rajdhani', sans-serif;
        }

        .rm-text-dark {
            color: var(--rm-dark);
            font-family: 'Segoe Script';
        }

        .rm-text-primary {
            color: var(--rm-primary);
            font-family: 'Segoe Script';
        }
        .rm-text-muted {
            color: var(--rm-muted);
        }

        .rm-text-medium {
            font-weight: 500;
        }

        .rm-text-bold {
            font-weight: 700;
        }

        .rm-footer {
            background: var(--rm-primary);
        }
        .menu{
            background-color: var(--rm-primary);
        }
        h3 {
        text-align: left;
        }
        a{
        text-decoration: none;
        color: rgba(34,54,69,.7);
        font-weight: 500;
        }
        a:hover{
            color: #970000;
        }
        ul{
            list-style-type: none;
        }
        nav{
            box-shadow: 0 0 10px rgba(0,0,0,0.8);
        }
        .navbar .navbar-brand a{
            padding: 1rem 0;
            text-decoration: none;
        }
        .navbar-toggler{
            background: #970000;
            border: none;
            padding: 10px 6px;
        }
        .navbar-toggler span{
            display: block;
            width: 22px;
            height: 2px;
            border: 1px;
            background: #fff;
        }
        .navbar-toggler span + span{
            margin-top: 4px;
            width: 18px;
            margin-left: 4px;
        }
        .navbar-toggler span + span + span {
            margin-top: 10px;
            margin-left: 1px;
        }
        .navbar-expand-lg .navbar-nav .nav-link{
            padding: 2rem 1.2rem;
            font-size: 1rem;
            position: relative;
        }
        .navbar-expand-lg .navbar-nav .nav-link:hover{
            border-top: 4px solid #970000;
        }
        .navbar-expand-lg .navbar-nav .nav-link:active{
            border-top: 4px solid #FFFFFF;
            color: #FFFFFF;
        }
        .logo{
            margin-left: 40px;
        }
        .card-header{
            background: var(--rm-primary);
        }
        .text-light{
            font-size: 0.8rem;
        }
    </style>
</head>
<body>
            <nav class="navbar navbar-expand-lg navbar-dark rm-bg-dark">
                <div class="container-fluid">
                    <a class="navbar-brand" href="#"><img src="imgramen/gareji.png" width="250" class="logo"></a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                    <div class="collapse navbar-collapse jstify-content-between" id="navbarSupportedContent">
                        <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" aria-current="page" href="/">HOME</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/about">ABOUT</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="/menu">MENU</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/services">SERVICES</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/contact">CONTACT</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/order">ORDER</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <!-- Hero -->
            <div class="container p-5">
                <div class="row">
                    <div class="col p-xl-5">
                        <h3 class="rm-display-3 rm-text-dark mt-5">Gareji <br><span class="rm-display-3 rm-text-primary">Ramen</span> </h3>     
                    </div>
                    <div class="col"> 
                        <img src="imgramen/menu.png" class="rounded mx-auto d-block" alt="" width="450" heigth="450">
                    </div>
                </div>
            </div>


                <div class="container px-4">
                    <div class="row">
                    @foreach ($users as $user)
                        <div class="col p-xl-5">
                            <div class="card" style="width: 24rem;">
                                <div class="card-header text-white">
                                    <h3 class="rm-display-20 text-center">{{ $user->kmenu }}</h3>
                                </div>

                                <div class="card-body">
                                    <img src="{{ url('imgramen/'.$user->img) }}" alt="" width="400" hiegth="600">
                                </div>
                            </div>      
                        </div>
                        @endforeach  
                    </div>
                </div>

        <br><br><br><br>
        <!-- Footer -->
        <div class="container-fluid rm-footer d-flex flex-column justify-content-center">
            <p class="text-light text-center"><br>Copyright &copy; 2022. All rights reserved. <br> Design by Angel Joy Manipon </p>
        </div> 
</body>
</html>