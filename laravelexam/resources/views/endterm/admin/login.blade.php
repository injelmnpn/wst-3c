<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>GAREJI RAMEN-login</title>
    <link rel="icon" href="imgramen/logo.png" type="image/gif/png/jpg/jpeg">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    <style>
        :root {
        --rm-primary: #970000;
        --rm-dark: #121314;
        --rm-light: #FFF;
        --rm-muted: #7D7D7D;
        }

        * {
            font-family: 'Rajdhani', sans-serif;
            font-size: 18px;
        }

        .rm-text-dark {
            color: var(--rm-light);
            font-family: 'Segoe Script';
            font-size: 2rem;
            font-weight: 700;
        }

        .rm-text-primary {
            color: var(--rm-primary);
            font-family: 'Segoe Script';
            font-size: 2rem;
            font-weight: 700;
        }
        .form-label{
            color: var(--rm-light);
            margin-left: 155px;
        }
        .btn{
            background-color:  #404040;
            border-color: #404040;
            padding: 10px;
            box-shadow: 2px 2px 2px #888888;
        }
        .btn:hover{
            background-color:  var(--rm-primary);
            border-color: var(--rm-primary);
        }
        .container{
            margin-top: 10px;
            box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
        }
        img {
            margin-top: 140px;
            border-radius: 20%;
            width: 230px;
            height: 230px;
            box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
        }
        .form-control2{
            width: 350px;
            height: 45px;
            border: auto;
            border-radius: 10px
        }
        .form-control1{
            width: 350px;
            margin-left: 10px;
            height: 45px;
            border: auto;
            border-radius: 10px
        }
    </style>
</head>
<body>
        <div class="container">
            <div class="row">
                <div class="col-6 bg-dark p-3">
                    <h3 class="rm-display-3 rm-text-dark mt-5 text-center">Gareji <br><span class="rm-display-3 rm-text-primary">Ramen</span> </h3>
                    <br>
				@if (count($errors) > 0)
				<div class = "alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>	
						@endforeach
					</ul>
				</div>
				@endif

				<?php
					echo Form::open(array('url'=>'/adminhome'));
				?>
              
                        <div class="row d-flex justify-content-center and align-items-center">
                            <p class="form-label">Username</p>
                        </div>
                        <div class="d-flex justify-content-center and align-items-center">
                            <div class="col-sm-8">
                                 <input type="text" class="form-control2" placeholder="Username" name="<?php echo 'username'; ?>">
                            </div>
                        </div>
                      
                        <br><br>

                        <div class="row d-flex justify-content-center and align-items-center">
                            <label for="exampleFormControlInput1" class="form-label">Password</label>
                        </div>

                        <div class="row d-flex justify-content-center and align-items-center">
                            <div class="col-sm-8">
                                 <input type="password" class="form-control1" placeholder="Password" name="<?php echo 'password'; ?>">
                            </div> 
                        </div>

                        <br><br>

                        <div class="d-grid gap-2 col-6 mx-auto">
                            <button class="btn btn-primary" type="submit" name="<?php echo 'Login'; ?>">Login</button>
                        </div>
				<?php
					echo Form::close();
				?>  
                        <br>
                </div>

                <div class="col-6 bg-light p-3 border">
                    <center>
                    <img src="imgramen/logo.jpg">
                    </center>
                </div>

            </div>
        </div>
</body>
</html>