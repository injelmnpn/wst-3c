<!DOCTYPE html>
<html>
<title>GAREJI RAMEN-admin</title>
<link rel="icon" href="imgramen/logo.png" type="image/gif/png/jpg/jpeg">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-colors-highway.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-colors-highway.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>
    <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>

    <script>
    $(document).ready(function() {
    $('#example').DataTable( {
        select: true
        } );
    } );
    </script>
<style>
       :root {
        --rm-primary: #970000;
        --rm-dark: #121314;
        --rm-light: #FFF;
        --rm-muted: #7D7D7D;
        }

        * {
            font-family: 'Rajdhani', sans-serif;
            font-size: 18px;
        }

        .rm-display-1 {
            font-family: 'Segoe Script';
        }
        a{
            font-size: 14px;
        }
        .btnn{
            margin-right: 5px;
            box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
            border-radius: 50%;
        }
        .logout{
            border-radius: 50%;
            box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
        }
</style>
<body>
<div class="w3-sidebar w3-black w3-bar-block w3-card w3-animate-left" style="width:20%">
    <img src="imgramen/logo.png" class="rounded mx-auto d-block" alt="" width="80" height="80">
    <figure class="text-center">
        <h3 class="rm-display-1">Gareji Ramen</h3>
    </figure>
    <br>
    <hr>
        <a href="/adminhome" class="w3-bar-item w3-button">Home</a>
    <hr>
        <a href="/adminabout" class="w3-bar-item w3-button">About Us</a>
    <hr>
        <a href="/adminmenu" class="w3-bar-item w3-button">Menu</a>
    <hr>
        <a href="/adminservice" class="w3-bar-item w3-button">Services</a>
    <hr>
        <a href="/admincontact" class="w3-bar-item w3-button">Contacts</a>
    <hr>
        <a href="/adminorder" class="w3-bar-item w3-button" style="background-color:grey;">Orders</a>
</div>
<br>
<div style="margin-left:21%">
    <div class='container'>
        <div class='row'>
            <div class='text-left'>
                <a href="/adminlogin" style="float: right;" class="logout"><img src="imgramen/power.png" alt="" width="40" height="40"></a>
            </div>
        </div>
    </div>
</div>
<div style="margin-left:21%">
    <div class="container">
    <p class="fs-4 fw-bold">ORDERS</p><a href="/order"><button type="button" class="btn btn-outline-danger">ORDER PAGE</button></a>
    <br><br>
        <table id="example" class="display">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">FULLNAME</th>
                    <th scope="col">NUMBER</th>
                    <th scope="col">ADDRESS</th>
                    <th scope="col">ORDER NAME</th>
                    <th scope="col">QTY</th>
                    <th scope="col">DATE</th>
                    <th scope="col">STATUS</th>
                    <th scope="col">ACTION</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($users as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->fullname }}</td>
                    <td>{{ $user->number }}</td>
                    <td>{{ $user->address }}</td>
                    <td>{{ $user->ordername }}</td>
                    <td>{{ $user->qty }}</td>
                    <td>{{ $user->date }}</td>
                    <td>{{ $user->status }}</td>
                    <td style="text-align:center"><a href="/oedit/{{ $user->id }}"><button type="button" class="btn btn-danger">Deliver/Cancel</button></i></a><br><br><a href="odelete/{{ $user->id }}"><button type="button" class="btn btn-dark">Delete</button></a></td>
                </tr>
                @endforeach  
            </tbody>
        </table>
    </div>
    <br>
</div>
</body>
</html>
