<!DOCTYPE html>
<html>
<title>GAREJI RAMEN-admin</title>
<link rel="icon" href="imgramen/logo.png" type="image/gif/png/jpg/jpeg">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-colors-highway.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
       :root {
        --rm-primary: #970000;
        --rm-dark: #121314;
        --rm-light: #FFF;
        --rm-muted: #7D7D7D;
        }

        * {
            font-family: 'Rajdhani', sans-serif;
            font-size: 18px;
        }

        .rm-display-1 {
            font-family: 'Segoe Script';
        }
        .button{
            height: 30px;
            width: 30px;
        }
        button{
            padding: 10px 16px;
            border-radius: 35px;
        }
        a{
            font-size: 14px;
            color: white;
            text-decoration: none;
        }
</style>
<body>
<div class="w3-sidebar w3-black w3-bar-block w3-card w3-animate-left" style="width:20%">
<br><br>
    <figure class="text-center">
        <h3 class="rm-display-1">Gareji Ramen</h3>
    </figure>
    <br>
    <hr>
        <a href="/adminhome" class="w3-bar-item w3-button">Home</a>
    <hr>
        <a href="/adminabout" class="w3-bar-item w3-button">About Us</a>
    <hr>
        <a href="/adminmenu" class="w3-bar-item w3-button">Menu</a>
    <hr>
        <a href="/adminservice" class="w3-bar-item w3-button">Services</a>
    <hr>
        <a href="/admincontact" class="w3-bar-item w3-button" style="background-color:grey;">Contacts</a>
</div>

<br>
    <div style="margin-left:25%">
        <div class='container'>
            <div class='row'>
                <div class='text-left'>
                    <a href="/admincontact"><button class="w3-button w3-highway-red" style="float: right;">Back</a>
                </div>
            </div>
        </div>
        <div class="container mt-3">
        <p class="fs-4 fw-bold">UPDATE CONTENTS</p>

            <form action = "/edit/<?php echo $users[0]->id; ?>" method = "post" enctype="multipart/form-data">
                <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">      
                        <div class="col-md-10">
                            <label for="formFile" class="form-label">Icon:</label>
                            <input class="form-control" type="file" id="formFile" name="icon" value = '<?php echo $users[0]->img; ?>' required>
                            <br>
                        </div>    
                                 
                        <div class="col-10">
                            <label for="inputname" class="form-label">Contacts:</label>
                            <input type="text" class="form-control" name="source" value = '<?php echo $users[0]->source; ?>' required>
                            <br>
                        </div>

                        <div class="col-10">
                            <label for="inputname" class="form-label">Information:</label>
                            <input type="text" class="form-control" name="info" value = '<?php echo $users[0]->info; ?>' required>
                            <br>
                        </div>
                        <div class="col-10">
                            <label for="inputname" class="form-label">URL:</label>
                            <input type="text" class="form-control" name="url" value = '<?php echo $users[0]->url; ?>' required>
                            <br>
                        </div>
<br>
                        <div class="d-grid gap-2 col-6 mx-auto">
                            <button type="submit" class="btn btn-dark mt-3 mb-3">Update Content</button>
                        </div>
                </form>
<br><br>
        </div>
    </div>       
</body>
</html>  