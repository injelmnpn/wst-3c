<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProductsInsertController extends Controller {

   public function insertproducts() {
    $users = DB::select('select * from products');
    return view('products',['users'=>$users]);
   }
	
   public function insertpro(Request $request) {
      $name = $request->input('name');
      $price = $request->input('price');
      $stocks = $request->input('stock');
      $date = $request->input('date');
      DB::insert('insert into products (name, price, stocks, date_created) values(?,?,?,?)',[$name, $price, $stocks, $date]);
    
      echo "<div class = 'alert alert-success'>
           Successfully Added
         </div>"; 
         $users = DB::select('select * from products');
    return view('products',['users'=>$users]);
   }

   public function fetch() {
    $users = DB::select('select * from products');
    return view('products',['users'=>$users]);
}
}
