<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class OrderViewController extends Controller {

    public function indexorder() {
        $users = DB::select('select * from ordering');
        return view('admin.admino',['users'=>$users]);
    }

    public function oshow($id) {
        $users = DB::select('select * from ordering where id = ?',[$id]);
        return view('admin.crudadmin.edito',['users'=>$users]);
    }

    public function oedit(Request $request,$id) {
        $name = $request->input('name');
        $num = $request->input('num');
        $add = $request->input('add');
        $order = $request->input('order');
        $qty = $request->input('qty');
        $date = $request->input('date');
        $status = $request->input('status');
        DB::update('update ordering set fullname = ?, number = ?, address = ?, ordername = ?, qty = ?, date = ?, status = ? where id = ?',[$name, $num, $add, $order, $qty, $date, $status, $id]);
        echo "<div class = 'alert alert-success'>
        Successfully Change
        </div>"; 
        $users = DB::select('select * from ordering where id = ?',[$id]);
        return view('admin.crudadmin.edito',['users'=>$users]);
    }

    public function odestroy($id) {
        DB::delete('delete from ordering where id = ?',[$id]);
        echo "<div class='alert alert-danger' role='alert'>Succesfully Deleted <a href = '/adminorder'>Click Here</a></div>";
    }
}