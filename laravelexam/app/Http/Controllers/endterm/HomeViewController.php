<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeViewController extends Controller {

    public function index() {
        $users = DB::select('select * from home');
        return view('admin.adminh',['users'=>$users]);
    }

    public function showw($id) {
        $users = DB::select('select * from home where id = ?',[$id]);
        return view('admin.crudadmin.edith',['users'=>$users]);
    }

    public function editt(Request $request,$id) {
        $name1 = $request->input('name1');
        $name2 = $request->input('name2');
        $tagline = $request->input('tagline');
        $message = $request->input('message');
        $imgs = $request->input('imgs');
        $messimg = $request->input('messimg');
        $address = $request->input('address');
        $logo = $request->input('logo');
        DB::update('update home set name1 = ?, name2 = ?, logo = ?, tagline = ?, messimg = ?, message = ?, img = ?, address = ? where id = ?',[$name1, $name2, $logo, $tagline, $messimg, $message, $imgs, $address, $id]);
        echo "<div class = 'alert alert-success'>
        Successfully Updated
        </div>"; 
        $users = DB::select('select * from home where id = ?',[$id]);
        return view('admin.crudadmin.edith',['users'=>$users]);
    }

    public function destroyy($id) {
        DB::delete('delete from home where id = ?',[$id]);
        echo "<div class='alert alert-danger' role='alert'>Succesfully Deleted <a href = '/adminhome'>Click Here</a></div>";
    }

    public function indexhome() {
        $users = DB::select('select * from home');
        return view('home',['users'=>$users]);
    }
}