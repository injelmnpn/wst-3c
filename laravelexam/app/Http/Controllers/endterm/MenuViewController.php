<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class MenuViewController extends Controller {

    public function index() {
        $users = DB::select('select * from menu');
        return view('admin.adminm',['users'=>$users]);
    }


    public function mshow($id) {
        $users = DB::select('select * from menu where id = ?',[$id]);
        return view('admin.crudadmin.editm',['users'=>$users]);
    }

    public function medit(Request $request,$id) {
        $kmenu = $request->input('kmenu');
        if($request->hasfile('image')){
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('imgramen', $filename);
   
            DB::update('update menu set img = ?, kmenu = ? where id = ?',[$filename, $kmenu, $id]);
        }
        echo "<div class = 'alert alert-success'>
        Successfully Updated
        </div>"; 
        $users = DB::select('select * from menu where id = ?',[$id]);
        return view('admin.crudadmin.editm',['users'=>$users]);
    }










    public function mdestroy($id) {
        DB::delete('delete from menu where id = ?',[$id]);
        echo "<div class='alert alert-danger' role='alert'>Succesfully Deleted <a href = '/adminmenu'>Click Here</a></div>";
    }

    public function indexmenu() {
        $users = DB::select('select * from menu');
        return view('menu',['users'=>$users]);
    }
}