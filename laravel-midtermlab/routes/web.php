<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/activity2', function () {
//      return view('activity2');
// });

// Route::get('/activity1', function () {
//     return view('activity1');
// });

// Route::get('/about', function () {
//     return view('about');
// });

// Route::get('/reg', function () {
//     return view('reg');
// });

// Route::get('/login', function () {
//     return view('login');
// });

// Route::get('/contact', function () {
//     return view('contact');
// });

// Route::get('/welcome', function(){
//     retuen view('welcome');
// });


// Route::get('/', function () {
//    return view('home');
// });

Route::get('/about', function () {
    return view('about');
});

Route::get('/menu', function () {
    return view('menu');
});

Route::get('/services', function () {
    return view('services');
});

Route::get('/contact', function () {
    return view('contact');
});

// Route::get('/dashboard', function () {
//     return view('dashboard');
// });








Route::get('/item/{itemno}/{name}/{price}', function ($itemno, $name, $price) {
    return "ITem No.: ". $itemno . "<br>" . "Name: " . $name . "<br>" . "Price: " . $price;
});

Route::get('/costumer/{id}/{name}/{address}/{age?}', function($id, $name, $address, $age=null){
    return "ID: ". $id . "<br>" . "Name: " . $name . "<br>" . "Address: " . $address . "<br>" . "Age: " . $age;
});

Route::get('/order/{customerid}/{name}/{orderno}/{date}', function ($customerid, $name, $orderno, $date) {
    return "Customer ID: ". $customerid . "<br>" . "Name: " . $name . "<br>" . "Order No. : " . $orderno . "<br>" . "Date: " . $date;
});

Route::get('/orderdetails/{transno}/{orderno}/{itemid}/{name}/{price}/{qty}/{receiptnum?}', function ($transno, $orderno, $itemid, $name, $price, $qty, $receiptnum=null) {
    return  "Trans No.: ". $transno . "<br>" . "Order No.: " . $orderno . "<br>" . "Item Id: " . $itemid . "<br>" . "Name: " . $name. "<br>" . "Price: " . $price. "<br>" . "Qty: " . $qty. "<br>" . "Receipt Num: " . $receiptnum. "<br>" . "TOTAL PRICE: " . $price * $qty;
});












// Route::get('/student',function()  
// {  
//   return view('student');  
// }); 


// Route::get('student/details',function()  
// {  
// 	$url="Name: Angel Joy B. Manipon" . "<br>" . "Section: 3C";  
// 	return $url;  

// })->name('student.details');  



// Route::redirect('/here', '/there', 404);



// Route::get('/student', function(){
//     return view('student');
// });

// Route::get('student/details',function()  
// {  
//     $url=route('student.details');  
//     return $url;  
// })->name('student.details');  

// Route::get('/about', function()  
// {  
//  return "This is a about us page";   
// });  

// Route::get('/post/{id}/{name}', function($id, $name)  
// {  
//     return "id number is : ". $id. " " . $name;   
// }  
// );  

// Route::get('user/{name?}', function ($name=null) {  
//     return $name;  
// });  
    
// Route::get('user/{name?}', function ($name = 'himani') {  
// 	    return $name;  
// });  
    

