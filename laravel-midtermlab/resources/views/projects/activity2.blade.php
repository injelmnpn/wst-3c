<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Portfolio-AM</title>
    <link rel="icon" href="image/logo.png" type="image/gif/png">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<style>
  :root {
    --rm-primary: #FF4C00;
    --rm-dark: #121314;
    --rm-light: #FFF;
    --rm-muted: #7D7D7D;
    --rm-blue: #82CAFA;
    --rm-bluee: #B4CFEC;
    --rm-wel: #0669b2;
    --rm-come: #1261A0;
    }
    img{
        margin-left: 40px;
    }
    a{
        text-decoration: none;
        color: rgba(34,54,69,.7);
        font-weight: 500;
    }
    a:hover{
        color: #ADD8E6;
    }
    ul{
        list-style-type: none;
    }
    nav{
      box-shadow: 0 0 10px rgba(0,0,0,0.8);
    }
    .navbar .navbar-brand a{
        padding: 1rem 0;
        text-decoration: none;
    }
    .navbar-toggler{
        background: #ADD8E6;
        border: none;
        padding: 10px 6px;
    }
    .navbar-toggler span{
        display: block;
        width: 22px;
        height: 2px;
        border: 1px;
        background: #fff;
    }
    .navbar-toggler span + span{
      margin-top: 4px;
      width: 18px;
      margin-left: 4px;
    }
    .navbar-toggler span + span + span {
      margin-top: 10px;
      margin-left: 1px;
    }
    .navbar-expand-lg .navbar-nav .nav-link{
        padding: 2rem 1.2rem;
        font-size: 1rem;
        position: relative;
    }
    .navbar-expand-lg .navbar-nav .nav-link:hover{
        border-top: 4px solid #ADD8E6;
    }
    .navbar-expand-lg .navbar-nav .nav-link:.active{
        border-top: 4px solid #ADD8E6;
        color: #ADD8E6;
    }
    .rm-hero {
    background: url(./assets/images/hero-1.jpg);
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    height: 70vh;
    }
    .img{
      margin-top: 100px;
      width: 350px;
      height: 350px;
    }
    .imagee{
      margin-left: 10px;
      margin-right: 10px;
      height: 570px;
    }
    .rm-bg-dark {
    background: var(--rm-light);
    }
    .rm-bg-light {
    background: var(--rm-bluee);
    }
    .rm-hero {
    background: url(image/gif2.jpg);
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    height: 86vh;
    margin-top: 20px;
    }
    .rm-heroo {
    background: url(image/gif.webp);
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    height: 80vh;
    }
    .rm-display-1 {
    font-size: 7rem;
    font-weight: 700;
    margin-top: 4px;
    
    }
    .rm-display-7 {
    font-size: 7rem;
    font-weight: 700;
    margin-top: 10px;
    }
    .rm-text-light {
    color: var(--rm-light);
    }
    .rm-text-primary {
    color: var(--rm-dark);
    }
    .rm-display-3 {
    font-size: 5rem;
    font-weight: 700;
    color: var(--rm-come);
    }
    .rm-display-9 {
    font-size: 5rem;
    font-weight: 700;
    color: var(--rm-blue);
    }
    .rm-display-2 {
    font-family: 'san-serif';
    font-size: 60px;
    font-weight: 700;
    margin-top: 150px;
    }
    .rm-display-4 {
    font-size: 16px;
    font-weight: normal;
    color: var(--rm-light);
    font-style: italic;
    }
    .card-img-top{
      margin-left: 0;
    }
    .footer-copyright {
    background: var(--rm-bluee);
    }
    .card-titlee{
      margin-top: 41px;
    }
    .foot{
      margin-bottom: 13px;
    }
</style>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="#"><img src="image/logo.png" width="70" heigth="70"></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span></span>
      <span></span>
      <span></span>
    </button>
    <div class="collapse navbar-collapse jstify-content-between" id="navbarSupportedContent">
      <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="#">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/about">About</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/reg">Registration</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/login">Login</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/contact">Contact</a>
        </li>
      </ul>
      <ul class="navbar-nav ms-auto">
      </ul>
    </div>
  </div>
</nav>


<div class="container-fluid rm-hero d-flex flex-column justify-content-center">
          <h1 class="rm-display-1 text-center rm-text-light">WEL<span class="rm-display-3 rm-text-primary">COME</span></h1>
          <h3 class="rm-display-4 text-center text-dark">Angel Manipon - Portfolio</h3>
</div>

<div class="container p-5">
    <div class="row">
        <div class="col p-xl-5">
            <h3 class="rm-display-3 rm-text-dark mt-5">H <span class="rm-display-9 rm-text-primary">I</span> </h3>
                <p class="rm-text-muted rm-text-medium">
                  Hi! Welcome to my page, I sincerely to welcome you. This Website was all about me and you will also communicate with me my friend. Please Enjoy and slowly to meet me. I hope you like my design and the layout of my website. Thanks to visit and you always welcome to this website.
                </p>
        </div>
              <div class="col"><img src="image/img2.jpg" alt="" class="img"></div>
    </div>
</div>

<div class="container-fluid rm-bg-dark pt-4">
    <div class="container-lg">
              <div class="row m-4">
                  <div class="col-lg p-4">
                    <div class="card" style="width: 18rem;">
                      <img class="card-img-top" src="image/gif2.gif">
                      <div class="card-body">
                        <h5 class="card-title">About</h5>
                        <p class="card-text">All some informations about me will posted in about page such as hobbies & interest, resume, description about me.</p>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg p-4">
                    <div class="card" style="width: 18rem;">
                      <img class="card-img-top" src="image/gif.webp">
                      <div class="card-body">
                        <h5 class="card-titlee">Login & Registration</h5>
                        <p class="card-text">You can Login and register from the Login and Registration pages that can we communicate will using my websites.</p>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg p-4">
                    <div class="card" style="width: 18rem;">
                      <img class="card-img-top" src="image/file.gif">
                      <div class="card-body">
                        <h5 class="card-title">Contact</h5>
                        <p class="card-text">You can also contact me from different platform of communication from the contact page.</p>
                      </div>
                    </div>
                  </div>
          </div>
    </div>
</div>

<br><br>
<div class="container-fluid rm-bg-light pt-4">
    <div class="container-lg">
      <div class="row justify-content-center">
        <div class="col-4" style="text-align: center;">
        <h3 class="rm-display-3 text-center text-light">SKILLS</span> </h3>
        </div>
      </div>  
      <br>
    <div class="row align-items-center">
      <div class="col" style="align: center;">
        <img src="image/ui.png" width="140" heigth="140">
        <div class="card-body">
          <h5 class="card-title">UI Design</h5>
            <p class="card-text">refers to the aesthetic elements by which people interact with a product, such as buttons, icons, menu bars, typography, colors, and more.</p>
        </div>
      </div>
      <div class="col" style="text-align: center;">
        <img src="image/time.png" width="120" heigth="120">
        <div class="card-body">
          <h5 class="card-title">Time Management</h5>
            <p class="card-text">organizing your time intelligently – so that you use it more effectively. The benefits of good time management include greater productivity, less stress, and more opportunities to do the things that matter. </p>
        </div>
      </div>
      <div class="col" style="text-align: right;">
       <img src="image/graphic.png" width="150" heigth="150">
       <div class="card-body">
          <h5 class="card-title">Graphic Design</h5>
            <p class="card-text">the craft of planning and creating visual content to communicate ideas and messages. Graphic design is everywhere you look — from billboards to cereal boxes to mobile apps. </p>
        </div>
      </div>
    </div>
    <br>
  </div>
</div>    



<footer class="page-footer font-small blue pt-4">
  <div class="container-fluid text-center text-md-left">
    <div class="row justify-content-center" style="background: var(--rm-blue);">
      <div class="col-2">
            <br>
            <a href="#!"><img src="image/fb.png" width="30" heigth="30"></a>
      </div>

      <div class="col-2">
            <br>
            <a href="#!"><img src="image/google.png" width="30" heigth="30"></a>
      </div>

      <div class="col-2">
            <br>
            <a href="#!"><img src="image/instagram.png" width="30" heigth="30" class="foot"></a>
      </div>
    </div>
  </div>

  <div class="footer-copyright text-center py-3">© 2022 Copyright:
    <a href=""> Designed by: Angel Joy B. Manipon | Power by <b>Laravel</b></a>
  </div>
</footer>
</body>
</html>
