<?php

namespace App\Http\Controllers;

use App\Models\Training;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;


class MainController extends Controller
{
    function dashboard(){

        $data = array(
            'list'=>DB::table('training')->get(),
        );
        

        return view('admin.dashboard', $data);
    }

    function addtraining(Request $request){

        $request -> validate([
            'training' => 'required',
            'date' => 'required',
        ]);

        $query = DB::table('training')->insert([
            'training'=> $request-> input('training'),
            'date'=> $request-> input('date'),
        ]);

        if($query){
            return back()->with('success', 'Email inserted');
        } else
            return back()->with('fail', 'Email not inserted');
    }

    function deletetraining($id){
        $query = DB::delete('DELETE FROM training WHERE training_id = ?', [$id]);

        if($query){
            return redirect()->back()->with('success', 'Data deleted');
        } else
            return redirect()->back()->with('fail', 'Data not deleted');

    }

    function edittraining($id){
        $edit = DB::select('select * from training where training_id = ?', [$id]);
        return view('admin.edit', ['edit' => $edit]);
    }

    function viewtraining($id){
        $id = request()->segment(2);
        $fetch = DB::select('select * from certificates where training_id = ?', [$id]);
        $data = array(
            'id'=>DB::table('training')->where('training_id', $id)->pluck('training'),
            'list'=>DB::table('training')->where('training_id', $id)->first()
        );
        return view('admin.generate', $data)->with('fetch', $fetch)->with('train', request()->segment(2));
    }

    function updatetraining(Request $request, $id)
    {
        $training = $request->get('training');
        $date = $request->get('date');
        DB::update('update training set training = "'.$training.'", date = "'.$date.'" where training_id = ?' ,[$id]);
        return redirect()->back()->with('success', 'Training Updated');
    }

    function searchCert(Request $request){

        $id = $request-> input('code');

        $query = DB::select('select * from certificates WHERE certificate_id = "'.$id.'";');

        if($query){
            return view ("admin/search")->with('fetch', $query);
        } else{
            return view ("admin/search")->with('fetch', $query);
        }
    }

    function searchUserCert(Request $request){

        $id = $request-> input('code');

        $query = DB::select('select * from certificates WHERE certificate_id = "'.$id.'";');

        return view ("search")->with('fetch', $query);
    }

    function userView(){
        return view('index');
    }
}
