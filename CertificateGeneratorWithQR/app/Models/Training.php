<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Training extends Model
{
    use HasFactory;
    public $primary_key = 'training_id';
    protected $fillable = [
      'training_id',
      'training',
      'date',
    ];
}
