@if (!isset(Auth::user()->username))
    <script>window.location = "/"</script>
@endif
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ url('../css/style.css') }}">
    <link href='https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css' rel='stylesheet'>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <title>Edit Training</title>
</head>
<body>
    <div class="justify-content-between align-items-center shadow nav">
        <div>
            <h2 style="margin: 0">Certificate Generator</h2>
        </div>

        <div class="d-flex">
            <ul class="navbar-nav">
                <li class="nav-item ">
                    @if(isset(Auth::user()->username))
                    <a class="nav-link">Welcome, {{ Auth::user()->username }}<span class="sr-only">(current)</span></a>
                    @endif
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/admin/logout" style="color: white;">Logout</a>
                </li>
            </ul>
        </div>
    </div>

    <div class="add">
        <h5 style="margin-bottom: 1.5rem;">Edit Seminar/Training Details</h5>
        @if (Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
            </div>
        @endif

        @foreach ($edit as $item)
            <form action="{{ route('update.training', $item->training_id) }}" method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-sm-3"> 
                        @csrf
                        <label for="training" class="form-label">Training/Seminar title</label>
                        <input type="text" class="form-control" value = "{{ $item->training }}" name="training" id="colFormLabel">
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-sm-3">
                        <label for="training" class="form-label">Date</label>
                        <input type="date" class="form-control" value = "{{ $item->date }}" name="date" id="colFormLabel"placeholder="Date">

                        <a href="{{ url('admin/dashboard') }}" class="btn btn-danger mt-4"><i class='bx by bx-block' ></i>Cancel</a>
                        <button type="submit" class="btn btn-warning mt-4" name="save"><i class='bx by bxs-edit-alt' ></i>Update</button>
                    </div>
                </div>
            </form>
        @endforeach
    </div>
</body>
<footer class="page-footer font-small justify-content-between align-items-center shadow nav fixed-bottom">

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">©{{ Date("Y") }} Copyright: PSU
    <a href="/" class="text-white"> </a>
  </div>
  <!-- Copyright -->

</footer>
</html>
